<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>FirstTimeSetup</name>
    <message>
        <location filename="../firsttimesetup.ui" line="20"/>
        <source>First Time Setup</source>
        <translation>Initiële setup</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="37"/>
        <source>Welcome to wfview!</source>
        <translation>Welkom bij wfview!</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="53"/>
        <source>How is your radio connected?</source>
        <translation>Hoe is je radio aangesloten?</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="59"/>
        <source>Serial Port on this PC</source>
        <translation>Seriële poort op deze PC</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="69"/>
        <source>USB Port on This PC</source>
        <translation>USB-poort op deze PC</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="79"/>
        <source>Ethernet Network</source>
        <translation>Ethernet netwerk</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="89"/>
        <source>WiFi Network</source>
        <translation>Wifi netwerk</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="111"/>
        <source>Next Steps:</source>
        <translation>Volgende stappen:</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="117"/>
        <source>You will now be taken to the Radio Access page under Settings.</source>
        <translation>Je gaat nu naar de Radio Toegangs pagina onder Instellingen</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="124"/>
        <source>Please fill in the relevant details for this connection type:</source>
        <translation>Vul hier de relevante details voor dit type verbinding:</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="163"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="170"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="177"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="188"/>
        <source>Note: If you do not have this information, press Exit Program, and return later.</source>
        <translation>Als je deze informatie niet weet, druk dan op Afsluiten en probeer het later</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="204"/>
        <source>Even if you have run wfview before, please take a moment to review your settings.</source>
        <translation>Zelfs als je wfview al eerder hebt gebruikt, neem even de tijd om uw instellingen te controleren</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="213"/>
        <source>Press to exit the program.
You will see this dialog box the next time you open wfview.</source>
        <translation>Druk om het programma af te sluiten
Je ziet dit scherm de volgende keer als je wfview start</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="217"/>
        <source>Exit Program</source>
        <translation>Afsluiten</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="224"/>
        <source>Press to skip the setup.</source>
        <translation>Druk om setup over te slaan</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="227"/>
        <source>Skip</source>
        <translation>Overslaan</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="234"/>
        <source>Press to go back to the prior step. </source>
        <translation>Druk om terug te gaan naar eerdere stappen</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="237"/>
        <source>Back</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="244"/>
        <source>Press for the next step.</source>
        <translation>Druk om verder te gaan</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="247"/>
        <location filename="../firsttimesetup.cpp" line="104"/>
        <source>Next</source>
        <translation>Verder</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="17"/>
        <source>Serial Port Name</source>
        <translation>Naam seriële poort</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="18"/>
        <source>Baud Rate</source>
        <translation>baudsnelheid</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="21"/>
        <source>Radio IP address, UDP Port Numbers</source>
        <translation>Radio IP-adres, UDP-poort nummers</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="22"/>
        <source>Radio Username, Radio Password</source>
        <translation>Radio gebruikersnaam, Radio wachtwoord</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="23"/>
        <source>Mic and Speaker on THIS PC</source>
        <translation>Microfoon en luidspreker op DEZE PC</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="44"/>
        <source>Finish</source>
        <translation>Einde</translation>
    </message>
</context>
<context>
    <name>aboutbox</name>
    <message>
        <location filename="../aboutbox.ui" line="14"/>
        <source>Form</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../aboutbox.ui" line="48"/>
        <source>wfview version</source>
        <translation>wfview versie</translation>
    </message>
    <message>
        <location filename="../aboutbox.ui" line="55"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;Detailed text here&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutbox.ui" line="69"/>
        <source>Build String</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>bandbuttons</name>
    <message>
        <location filename="../bandbuttons.ui" line="14"/>
        <source>Form</source>
        <translation>Band Selectie</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="20"/>
        <source>Band</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="49"/>
        <source>2200m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="71"/>
        <source>630m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="93"/>
        <source>160m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="96"/>
        <source>L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="118"/>
        <source>80m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="121"/>
        <source>8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="147"/>
        <source>60m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="150"/>
        <source>S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="172"/>
        <source>40m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="175"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="197"/>
        <source>30m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="200"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="222"/>
        <source>20m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="225"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="251"/>
        <source>17m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="254"/>
        <source>7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="276"/>
        <source>15m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="279"/>
        <source>5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="301"/>
        <source>12m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="304"/>
        <source>T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="326"/>
        <source>10m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="329"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="355"/>
        <source>6m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="358"/>
        <source>6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="380"/>
        <source>4m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="383"/>
        <source>$</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="405"/>
        <source>2m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="408"/>
        <source>V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="430"/>
        <source>70cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="433"/>
        <source>U</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="455"/>
        <source>23cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="484"/>
        <source>13cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="506"/>
        <source>6cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="528"/>
        <source>3cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="554"/>
        <source>WFM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="557"/>
        <source>W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="579"/>
        <source>Air</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="582"/>
        <source>A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="604"/>
        <source>Gen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="607"/>
        <source>G</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="625"/>
        <source>Segment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="631"/>
        <source>&amp;Last Used</source>
        <translation>&amp;Laatste gebruikt</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="647"/>
        <source>Band Stack Selection:</source>
        <translation>Selecteer Band Stack</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="655"/>
        <source>1 - Latest Used</source>
        <translation>1   Laatst Gebruikt</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="660"/>
        <source>2 - Older</source>
        <translation>2   Ouder</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="665"/>
        <source>3 - Oldest Used</source>
        <translation>3   Oudste Gebruikt</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="673"/>
        <source>Voice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="680"/>
        <source>Data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="687"/>
        <source>&amp;CW</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="694"/>
        <source>Use this button to set the current bandstack register frequency/mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="697"/>
        <source>Set to current freq/mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="717"/>
        <source>Work on Sub Band</source>
        <translation>Werken op Sub Band</translation>
    </message>
</context>
<context>
    <name>calibrationWindow</name>
    <message>
        <location filename="../calibrationwindow.ui" line="32"/>
        <location filename="../calibrationwindow.ui" line="56"/>
        <source>Reference Adjustment</source>
        <translation>Referentie Aanpassing</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="49"/>
        <source>IC-9700</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="69"/>
        <source>Course</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="76"/>
        <source>Fine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="163"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Save the calibration data to the indicated slot in the preference file. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sla de kalibratiegegevens op in het aangegeven slot in het voorkeurenbestand. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="166"/>
        <source>Save</source>
        <translation>Opslaan</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="176"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Load the calibration data from the indicated slot in the preference file. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Laad de kalibratiegegevens uit het aangegeven slot in het voorkeurenbestand. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="179"/>
        <source>Load</source>
        <translation>Laden</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="189"/>
        <source>Slot:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="200"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="205"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="210"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="215"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="220"/>
        <source>5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="243"/>
        <source>Read Current Rig Calibration</source>
        <translation>Laad de huidige rig-kalibratie</translation>
    </message>
</context>
<context>
    <name>controllerSetup</name>
    <message>
        <location filename="../controllersetup.ui" line="14"/>
        <source>Controller setup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="27"/>
        <source>Tab 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="32"/>
        <source>Tab 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="57"/>
        <source>Backup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="64"/>
        <source>Restore</source>
        <translation>Herstellen</translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="84"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="91"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
</context>
<context>
    <name>cwSender</name>
    <message>
        <location filename="../cwsender.ui" line="14"/>
        <source>MainWindow</source>
        <translation>CW</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="17"/>
        <source>Send the text in the edit box</source>
        <translation>Stuur de tekst in het invoerveld</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="24"/>
        <source>Macros</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="30"/>
        <location filename="../cwsender.ui" line="40"/>
        <location filename="../cwsender.ui" line="50"/>
        <location filename="../cwsender.ui" line="70"/>
        <location filename="../cwsender.ui" line="80"/>
        <location filename="../cwsender.ui" line="90"/>
        <location filename="../cwsender.ui" line="100"/>
        <location filename="../cwsender.ui" line="110"/>
        <location filename="../cwsender.ui" line="120"/>
        <location filename="../cwsender.ui" line="130"/>
        <source>Macro Access Button</source>
        <translation>Macro toegangs knop</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="33"/>
        <source>Macro 5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="43"/>
        <source>Macro 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="53"/>
        <source>Macro 4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="60"/>
        <source>Check this box to enter edit mode, where you can then press the macro buttons to edit the macros.</source>
        <translation>Klik hier om in wijzig mode te komen. Je kunt nu op de macro knop drukken en de macro wijzigen.</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="63"/>
        <source>Edit Mode</source>
        <translation>Wijzig Mode</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="73"/>
        <source>Macro 7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="83"/>
        <source>Macro 3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="93"/>
        <source>Macro 6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="103"/>
        <source>Macro 10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="113"/>
        <source>Macro 9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="123"/>
        <source>Macro 8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="133"/>
        <source>Macro 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="142"/>
        <source>Seq</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="149"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sequence number, for contests. &lt;/p&gt;&lt;p&gt;Substitute &amp;quot;%1&amp;quot; in your macro text to use it. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Opvolgende nummers, voor contests. &lt;/p&gt;&lt;p&gt;Vervang &amp;quot;%1&amp;quot; in in je macrotekst om het te gebruiken. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="170"/>
        <source>CW Transmission Transcript</source>
        <translation>CW-transmissietranscript</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="211"/>
        <source>Stop sending CW</source>
        <translation>Stop CW versturen</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="217"/>
        <source>Stop</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="224"/>
        <source>Local Sidetone Level</source>
        <translation>Lokaal zijtoon niveau</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="231"/>
        <source>Local sidetone generator volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="244"/>
        <source>Enable local sidetone generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="247"/>
        <source>Enable</source>
        <translation>Inschakelen</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="282"/>
        <source>Send</source>
        <translation>Versturen</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="285"/>
        <source>Return</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="301"/>
        <source>Type here to send text as CW</source>
        <translation>Typ hier om tekst als CW te verzenden</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="335"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the desired break-in mode:&lt;/p&gt;&lt;p&gt;1. None: You must manually key and unkey the radio.&lt;/p&gt;&lt;p&gt;2. Semi: Transmit is automatic and switches to receive at the end of the text.&lt;/p&gt;&lt;p&gt;3. Full: Same as semi, but with breaks between characters when possible.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stel de gewenste break in modus in:&lt;/p&gt;&lt;p&gt;1. Geen: U moet de radio handmatig in- en uitschakelen.&lt;/p&gt;&lt;p&gt;2. Semi: Zenden gebeurt automatisch en schakelt over naar ontvangen aan het einde van de tekst.&lt;/p&gt;&lt;p&gt;3. Volledig: hetzelfde als semi, maar indien mogelijk met pauzes tussen tekens.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="339"/>
        <source>Off</source>
        <translation>Uit</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="344"/>
        <source>Semi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="349"/>
        <source>Full</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="360"/>
        <source>PITCH  (Hz):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="370"/>
        <source>WPM:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="383"/>
        <source>Sets the pitch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="408"/>
        <source>Break In</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="421"/>
        <source>Sets the dash ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="446"/>
        <source>Dash Ratio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="459"/>
        <source>Set the Words Per Minute</source>
        <translation>Stel de woorden per minuut in</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="475"/>
        <source>Replace numbers with short letters, for example 9 becomes N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="478"/>
        <source>Cut Num</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="485"/>
        <source>Send immediately: Don&apos;t wait for enter, send characters as they are typed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="488"/>
        <source>Send Immed</source>
        <translation>Direct verzenden</translation>
    </message>
</context>
<context>
    <name>debugWindow</name>
    <message>
        <location filename="../debugwindow.ui" line="14"/>
        <source>Dialog</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="22"/>
        <source>Current cache items in cachingView()</source>
        <translation>Huidige cache-items in cachingView()</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="32"/>
        <source>Current queue items in cachingView()</source>
        <translation>Huidige queue-items in cachingView()</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="67"/>
        <location filename="../debugwindow.ui" line="107"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="72"/>
        <location filename="../debugwindow.ui" line="112"/>
        <source>Function</source>
        <translation>Functie</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="77"/>
        <location filename="../debugwindow.ui" line="132"/>
        <source>Value</source>
        <translation>Waarde</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="82"/>
        <location filename="../debugwindow.ui" line="127"/>
        <source>RX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="87"/>
        <source>Request</source>
        <translation>Aanvraag</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="92"/>
        <source>Reply</source>
        <translation>Antwoord</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="117"/>
        <source>Priority</source>
        <translation>Prio</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="122"/>
        <source>Get/Set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="137"/>
        <source>Recurring</source>
        <translation>Herhalend</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="149"/>
        <location filename="../debugwindow.ui" line="186"/>
        <source>Pause refresh</source>
        <translation>Pauzeer vernieuwen</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="169"/>
        <location filename="../debugwindow.ui" line="206"/>
        <source>Refresh Interval (ms)</source>
        <translation>Vernieuwingsinterval (ms)</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="176"/>
        <location filename="../debugwindow.ui" line="219"/>
        <source>99999</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="179"/>
        <source>500</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="222"/>
        <source>1000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="245"/>
        <source>Scroll test:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="274"/>
        <source>TextLabel</source>
        <translation>TekstLabel</translation>
    </message>
</context>
<context>
    <name>frequencyinputwidget</name>
    <message>
        <location filename="../frequencyinputwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Frequentie input</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="22"/>
        <source>Frequency:</source>
        <translation>Frequentie:</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="33"/>
        <source>Go</source>
        <translation>Ga</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="36"/>
        <source>Return</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="48"/>
        <source>Entry</source>
        <translation>Invoeren</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="66"/>
        <location filename="../frequencyinputwidget.ui" line="69"/>
        <source>5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="88"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To recall a preset memory:&lt;/p&gt;&lt;p&gt;1. Type in the preset number (0 through 99)&lt;/p&gt;&lt;p&gt;2. Press RCL (or use hotkey &amp;quot;R&amp;quot;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Een vooraf ingesteld geheugen oproepen:&lt;/p&gt;&lt;p&gt;1. Voer het voorkeuzenummer in (0 tot en met 99)&lt;/p&gt;&lt;p&gt;2. Druk op RCL (of gebruik de sneltoets &amp;quot;R&amp;quot;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="91"/>
        <source>&amp;RCL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="94"/>
        <source>R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="113"/>
        <location filename="../frequencyinputwidget.ui" line="116"/>
        <source>6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="135"/>
        <location filename="../frequencyinputwidget.ui" line="138"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="160"/>
        <source>&amp;CE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="163"/>
        <source>C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="182"/>
        <location filename="../frequencyinputwidget.ui" line="185"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="204"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To store a preset:&lt;/p&gt;&lt;p&gt;1. Set the desired frequency and mode per normal methods&lt;/p&gt;&lt;p&gt;2. Type the index to to store to (0 through 99)&lt;/p&gt;&lt;p&gt;3. Press STO (or use hotkey &amp;quot;S&amp;quot;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Een geheugen opslaan:&lt;/p&gt;&lt;p&gt;1. Stel de gewenste frequentie en modus in volgens de normale methoden&lt;/p&gt;&lt;p&gt;2. Typ de index om op te slaan in (0 tot en met 99)&lt;/p&gt;&lt;p&gt;3. Druk op STO (of gebruik de sneltoets &amp;quot;S&amp;quot;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="207"/>
        <source>&amp;STO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="210"/>
        <source>S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="229"/>
        <location filename="../frequencyinputwidget.ui" line="232"/>
        <source>9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="251"/>
        <source>Back</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="254"/>
        <source>Backspace</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="273"/>
        <location filename="../frequencyinputwidget.ui" line="276"/>
        <source>Enter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="295"/>
        <location filename="../frequencyinputwidget.ui" line="298"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="317"/>
        <location filename="../frequencyinputwidget.ui" line="320"/>
        <source>.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="339"/>
        <location filename="../frequencyinputwidget.ui" line="342"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="361"/>
        <location filename="../frequencyinputwidget.ui" line="364"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="383"/>
        <location filename="../frequencyinputwidget.ui" line="386"/>
        <source>7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="405"/>
        <location filename="../frequencyinputwidget.ui" line="408"/>
        <source>8</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>loggingWindow</name>
    <message>
        <location filename="../loggingwindow.ui" line="14"/>
        <source>Form</source>
        <translation>Logging</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="56"/>
        <source>Annotation:</source>
        <translation>Annotatie</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="69"/>
        <source>You may enter your own log notes here.</source>
        <translation>U kunt hier uw eigen lognotities invoeren.</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="88"/>
        <source>Adds user-text to the log.</source>
        <translation>Voegt gebruikerstekst toe aan het logboek.</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="91"/>
        <source>Annotate</source>
        <translation>Annoteren</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="108"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable or disable debug logging. Use the &amp;quot;-d&amp;quot; or &amp;quot;--debug&amp;quot; flag to open wfview with debug logging enabled on startup. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Debug-logboekregistratie in- of uitschakelen. Gebruik de &amp;quot;-d&amp;quot; of &quot;--debuggen&quot; flag om wfview te openen met foutopsporingsregistratie ingeschakeld bij het opstarten. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="111"/>
        <source>Debug</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="118"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This enables the logging of nearly all CI-V traffic. &lt;span style=&quot; font-weight:600;&quot;&gt;Use with caution&lt;/span&gt;. It is a lot of data. Meter levels and scope data are not shown. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="121"/>
        <source>CommDebug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="128"/>
        <source>RigCtl Debug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="135"/>
        <source>Scroll to bottom</source>
        <translation>Scroll naar einde</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="138"/>
        <source>Scroll Down</source>
        <translation>Scroll naar beneden</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="145"/>
        <source>Clears the display. Does not clear the log file.</source>
        <translation>Wist het scherm. Wist het logbestand niet.</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="148"/>
        <source>Clear</source>
        <translation>Wissen</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="155"/>
        <source>Makes a best-effort to ask the host system to open the log file directory.</source>
        <translation>Vraagt het hostsysteem de map met logbestanden te openen.</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="158"/>
        <source>Open Log Directory</source>
        <translation>Open de logdirectory</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="165"/>
        <source>Makes a best-effort to ask the host system to open the logfile.</source>
        <translation>Vraagt het hostsysteem het logbestand te openen.</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="168"/>
        <source>Open Log</source>
        <translation>Open logbestand</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="175"/>
        <source>Copy the path of the log file to your clipboard.</source>
        <translation>Kopieer het pad van het logbestand naar uw klembord.</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="178"/>
        <source>Copy Path</source>
        <translation>Kopieer pad</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="185"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sends text to termbin.com. Some personal information (such as your username) is in the log file, so do not click this button unless you are ok sharing your log file. This is a quick way to receive a URL, pointing to your log file text, that you can send to other people. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Verzendt tekst naar termbin.com. Sommige persoonlijke informatie (zoals uw gebruikersnaam) bevindt zich in het logbestand, dus klik niet op deze knop tenzij u akkoord gaat met het delen van uw logbestand. Dit is een snelle manier om een ​​URL te ontvangen die naar de tekst van uw logbestand verwijst, die u naar andere mensen kunt verzenden. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="188"/>
        <source>Send to termbin.com</source>
        <translation>Verzenden naar termbin.com</translation>
    </message>
</context>
<context>
    <name>memories</name>
    <message>
        <location filename="../memories.ui" line="14"/>
        <source>Memory Management</source>
        <translation>Geheugen Management</translation>
    </message>
    <message>
        <location filename="../memories.ui" line="43"/>
        <source>Disable Editing</source>
        <translation>Schakel bewerken uit</translation>
    </message>
    <message>
        <location filename="../memories.ui" line="66"/>
        <source>Start Scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memories.ui" line="123"/>
        <source>Select Memory Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Loading Memories (this may take a while!)</source>
        <translation type="vanished">Geheugens laden (dit kan even duren!)</translation>
    </message>
    <message>
        <location filename="../memories.ui" line="86"/>
        <source>.csv Import</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../memories.ui" line="93"/>
        <source>All fields</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memories.ui" line="103"/>
        <source>.csv Export</source>
        <translation></translation>
    </message>
    <message>
        <source>Memory Mode</source>
        <translation type="vanished">Geheugenmodus</translation>
    </message>
    <message>
        <source>VFO Mode</source>
        <translation type="vanished">VFO-modus</translation>
    </message>
</context>
<context>
    <name>meter</name>
    <message>
        <location filename="../meter.cpp" line="314"/>
        <source>Double-click to set meter</source>
        <translation>Dubbelklik om de meter in te stellen</translation>
    </message>
</context>
<context>
    <name>pttyHandler</name>
    <message>
        <location filename="../pttyhandler.cpp" line="205"/>
        <source>Read failed: %1</source>
        <translation>Lezen mislukt: %1</translation>
    </message>
</context>
<context>
    <name>receiverWidget</name>
    <message>
        <location filename="../receiverwidget.cpp" line="31"/>
        <location filename="../receiverwidget.cpp" line="46"/>
        <source>VFO A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="44"/>
        <source>VFO B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="50"/>
        <source>A&lt;&gt;B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="60"/>
        <source>A=B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="70"/>
        <source>SPLIT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="133"/>
        <source>Detach</source>
        <translation>Loskoppelen</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="135"/>
        <source>Detach/re-attach scope from main window</source>
        <translation>Maak de scoop los/bevestig deze opnieuw aan het hoofdvenster</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="139"/>
        <source>Spectrum Mode</source>
        <translation>Spectrum Modus</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="142"/>
        <source>Spectrum Span</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="146"/>
        <source>Spectrum Edge</source>
        <translation>Spectrum Rand</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="147"/>
        <source>Custom Edge</source>
        <translation>Aangepaste Rand</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="148"/>
        <source>Define a custom (fixed) scope edge</source>
        <translation>Definieer een aangepaste (vaste) scope rand</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="149"/>
        <source>To Fixed</source>
        <translation>Naar vast</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="150"/>
        <source>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Press button to convert center mode spectrum to fixed mode, preserving the range. This allows you to tune without the spectrum moving, in the same currently-visible range that you see now. &amp;lt;/p&amp;gt;&amp;lt;p&amp;gt;&amp;lt;br/&amp;gt;&amp;lt;/p&amp;gt;&amp;lt;p&amp;gt;The currently-selected edge slot will be overridden.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Druk op de knop om het middenmodusspectrum naar de vaste modus te converteren, waarbij het bereik behouden blijft. Hierdoor kunt u afstemmen zonder dat het spectrum beweegt, in hetzelfde momenteel zichtbare bereik dat u nu ziet. &amp;lt;/p&amp;gt;&amp;lt;p&amp;gt;&amp;lt;br/&amp;gt;&amp;lt;/p&amp;gt;&amp;lt;p&amp;gt;Het momenteel geselecteerde edge-slot wordt overschreven.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="162"/>
        <location filename="../receiverwidget.cpp" line="164"/>
        <source>Configure Scope</source>
        <translation>Configureer Scope</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="163"/>
        <source>Change various settings of the current Scope</source>
        <translation>Wijzig verschillende instellingen van de huidige Scope</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="214"/>
        <source>Center Mode</source>
        <translation>Center Modus</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="215"/>
        <source>Fixed Mode</source>
        <translation>Fixed Modus</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="216"/>
        <source>Scroll-C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="217"/>
        <source>Scroll-F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="220"/>
        <source>Fixed Edge 1</source>
        <translation>Vaste Rand 1</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="220"/>
        <source>Fixed Edge 2</source>
        <translation>Vaste Rand 2</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="220"/>
        <source>Fixed Edge 3</source>
        <translation>Vaste Rand 3</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="220"/>
        <source>Fixed Edge 4</source>
        <translation>Vaste Rand 4</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="249"/>
        <source>SCOPE OUT OF RANGE</source>
        <translation>SCOPE BUITEN BEREIK</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="260"/>
        <source> OVF </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="325"/>
        <source>Scope display reference</source>
        <translation>Scope-weergavereferentie</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="326"/>
        <source>Selects the display reference for the Scope display</source>
        <translation>Selecteert de weergavereferentie voor de Scope-weergave</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="327"/>
        <source>Select display reference of scope</source>
        <translation>Selecteer de weergavereferentie van de scope</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="328"/>
        <source>Ref</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="332"/>
        <source>Length</source>
        <translation>Lengte</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="337"/>
        <source>Scope display ceiling</source>
        <translation>Scope-displayplafond</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="338"/>
        <source>Selects the display ceiling for the Scope display</source>
        <translation>Selecteert het displayplafond voor het Scope-display</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="339"/>
        <source>Select display ceiling of scope</source>
        <translation>Selecteer het displayplafond van de scope</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="340"/>
        <source>Ceiling</source>
        <translation>Plafond</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="345"/>
        <source>Scope display floor</source>
        <translation>Scope-displayvloer</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="346"/>
        <source>Selects the display floor for the Scope display</source>
        <translation>Selecteert de displayvloer voor de Scope-weergave</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="347"/>
        <source>Select display floor of scope</source>
        <translation>Selecteer de scope vloer van het bereik</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="348"/>
        <source>Floor</source>
        <translation>Vloer</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="351"/>
        <source>Speed Fast</source>
        <translation>Hoge Snelheid</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="352"/>
        <source>Speed Mid</source>
        <translation>Gemiddelde Snelheid</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="353"/>
        <source>Speed Slow</source>
        <translation>Lage Snelheid</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="355"/>
        <source>Waterfall display speed</source>
        <translation>Waterval scherm snelheid</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="356"/>
        <source>Selects the speed for the waterfall display</source>
        <translation>Selecteer de snelheid voor de waterval display</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="357"/>
        <source>Waterfall Speed</source>
        <translation>Waterval Snelheid</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="358"/>
        <source>Speed</source>
        <translation>Snelheid</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="361"/>
        <source>Waterfall display color theme</source>
        <translation>Waterval display kleurenschema</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="362"/>
        <source>Selects the color theme for the waterfall display</source>
        <translation>Selecteert het kleurthema voor de watervalweergave</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="363"/>
        <source>Waterfall color theme</source>
        <translation>Waterval kleurenthema</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="377"/>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="381"/>
        <source>PBT Inner</source>
        <translation>PBT binnen</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="385"/>
        <source>PBT Outer</source>
        <translation>PBT buiten</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="399"/>
        <source>IF Shift</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="403"/>
        <source>Fill Width</source>
        <translation>Vulbreedte</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="1244"/>
        <source>Scope Edges</source>
        <translation>Scope Randen</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="1245"/>
        <source>Please enter desired scope edges, in MHz,
with a comma between the low and high range.</source>
        <translation>Voer de gewenste scoopranden in, in MHz,
met een komma tussen het lage en hoge bereik.</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="1273"/>
        <source>Error, could not interpret your input.                          &lt;br/&gt;Please make sure to place a comma between the frequencies.                          &lt;br/&gt;For example: &apos;7.200, 7.300&apos;</source>
        <translation>Fout. Kan uw invoer niet interpreteren. &lt;br/&gt;Zorg ervoor dat u een komma tussen de frequenties plaatst. &lt;br/&gt;Bijvoorbeeld: &apos;7.200, 7.300&apos;</translation>
    </message>
</context>
<context>
    <name>repeaterSetup</name>
    <message>
        <location filename="../repeatersetup.ui" line="26"/>
        <source>Repeater Setup</source>
        <translation>Repeater-instelling</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="47"/>
        <source>Repeater Duplex</source>
        <translation>Repeater-duplex</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="53"/>
        <source>Simplex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="63"/>
        <source>Dup+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="73"/>
        <source>Dup-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="83"/>
        <source>Auto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="95"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the repeater offset for radios using Repeater modes. Only available on radios that have repeater modes. Radios using Split should instead use the provided Split Mode section with a custom Offset. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stel de repeater-offset in voor radio&apos;s die Repeater-modi gebruiken. Alleen beschikbaar op radio&apos;s met repeatermodi. Radio&apos;s die Split gebruiken, moeten in plaats daarvan de meegeleverde Split Mode-sectie gebruiken met een aangepaste Offset. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="98"/>
        <source>Set Offset (MHz):</source>
        <translation>Offset instellen (MHz):</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="107"/>
        <source>Rpt Offset (MHz)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="123"/>
        <source>Split Mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="131"/>
        <source>Turn on Split</source>
        <translation>Schakel Split in</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="134"/>
        <source>Split On</source>
        <translation>Split aan</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="144"/>
        <source>Turn off Split</source>
        <translation>Zet Split uit</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="147"/>
        <source>Split Off</source>
        <translation>Split uit</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="157"/>
        <source>QuickSplit</source>
        <translation>SnelSplit</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="168"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Click here to automatically set the sub VFO (transmit VFO) tone. Only available on some radios. Other radios may take care of this for you.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Klik hier om automatisch de sub-VFO-toon (zend-VFO) in te stellen. Alleen beschikbaar op sommige radio&apos;s. Andere radio&apos;s kunnen dit mogelijk voor u regelen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="171"/>
        <source>Set Rpt Tone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="181"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Click here to continually set the transmit VFO to match the receive VFO with the offset accounted for.  &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Klik hier om de zend-VFO continu in te stellen zodat deze overeenkomt met de ontvangst-VFO met betreffende offset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="184"/>
        <source>AutoTrack</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="195"/>
        <source>Offset (KHz):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="214"/>
        <source>Enter the desired split offset in KHz.</source>
        <translation>Voer de gewenste split-offset in KHz in.</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="227"/>
        <location filename="../repeatersetup.ui" line="243"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the transmit frequency to the receive frequency PLUS the offset. Sets the radio sub VFO and also populates the wfview text box (as a convenience). &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stel de zendfrequentie in op de ontvangstfrequentie PLUS de offset. Stelt de radio-sub-VFO in en vult ook het wfview-tekstvak in (voor het gemak). &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="230"/>
        <source>Split+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="246"/>
        <source>Split-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="257"/>
        <source>Tx Freq (MHz):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="286"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the transmit frequency manually. Not needed if the Split+ or Split- button was used. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stel de zendfrequentie handmatig in. Niet nodig als de knop Split+ of Split- wordt gebruikt. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="289"/>
        <source>Set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="307"/>
        <source>VFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="313"/>
        <source>Swap VFO A with VFO B. Some radios do not support this.</source>
        <translation>Verwissel VFO A met VFO B. Sommige radio&apos;s ondersteunen dit niet.</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="316"/>
        <source>Swap AB</source>
        <translation>Wissel AB</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="323"/>
        <source>Select the Sub VFO</source>
        <translation>Selecteer de Sub-VFO</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="326"/>
        <source>Sel Sub</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="333"/>
        <source>Select the Main VFO</source>
        <translation>Selecteer de hoofd-VFO</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="336"/>
        <source>Sel Main</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="343"/>
        <source>Select VFO B</source>
        <translation>Selecteer VFO-B</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="346"/>
        <source>Sel B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="353"/>
        <source>Select VFO A</source>
        <translation>Selecteer VFO-A</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="356"/>
        <source>Sel A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="363"/>
        <source>Set VFO B to VFO A</source>
        <translation>Stel VFO B in op VFO A</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="366"/>
        <source>A=B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="373"/>
        <source>Set the SUB VFO to match the Main VFO</source>
        <translation>Stel de SUB VFO zo in dat deze overeenkomt met de hoofd-VFO</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="376"/>
        <source>M=&gt;S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="383"/>
        <source>Swap the Main VFO and Sub VFO</source>
        <translation>Wissel de hoofd-VFO en de sub-VFO om</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="386"/>
        <source>Swap MS</source>
        <translation>Wissel MS</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="402"/>
        <source>Repeater Tone Type</source>
        <translation>Repeater Toon Type</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="408"/>
        <source>Only available in FM</source>
        <translation>Alleen beschikbaar in FM</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="415"/>
        <source>None</source>
        <translation>Geen</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="425"/>
        <source>Transmit Tone only</source>
        <translation>Alleen zend toon</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="435"/>
        <source>Tone Squelch</source>
        <translation>Toon Squelch</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="445"/>
        <location filename="../repeatersetup.ui" line="496"/>
        <source>DTCS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="455"/>
        <source>Set the Tone Mode for the Sub VFO. Not available on all radios.</source>
        <translation>Stel de toonmodus voor de sub-VFO in. Niet op alle radio&apos;s beschikbaar.</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="458"/>
        <location filename="../repeatersetup.ui" line="529"/>
        <source>Set Sub VFO</source>
        <translation>Sub-VFO instellen</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="474"/>
        <source>Tone Selection</source>
        <translation>Toon Selectie</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="482"/>
        <source>Tone</source>
        <translation>Toon</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="510"/>
        <source>Invert Tx</source>
        <translation>Tx omkeren</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="517"/>
        <source>Invert Rx</source>
        <translation>RX omkeren</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="526"/>
        <source>Set the Sub VFO to the selected Tone. Not available on all radios and not available for DTCS.</source>
        <translation>Stel de Sub VFO in op de geselecteerde toon. Niet op alle radio&apos;s beschikbaar en niet beschikbaar voor DTCS.</translation>
    </message>
</context>
<context>
    <name>rigCreator</name>
    <message>
        <location filename="../rigcreator.ui" line="20"/>
        <source>Rig Creator</source>
        <translation>Rig Maker</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="32"/>
        <source>Memories</source>
        <translation>Geheugens</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="40"/>
        <source>Grp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="47"/>
        <location filename="../rigcreator.ui" line="1168"/>
        <source>Mem</source>
        <translation>Geh</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="54"/>
        <location filename="../rigcreator.ui" line="61"/>
        <location filename="../rigcreator.ui" line="68"/>
        <location filename="../rigcreator.ui" line="89"/>
        <source>999</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="75"/>
        <source>Sat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="82"/>
        <location filename="../rigcreator.ui" line="1153"/>
        <source>Start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="101"/>
        <source>Commands</source>
        <translation>Commando&apos;s</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="156"/>
        <location filename="../rigcreator.ui" line="275"/>
        <source>Command</source>
        <translation>Commando</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="161"/>
        <source>String</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="166"/>
        <source>Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="171"/>
        <source>Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="176"/>
        <source>29</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="181"/>
        <source>G/S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="198"/>
        <source>Preamps</source>
        <translation>Voorversterkers</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="241"/>
        <location filename="../rigcreator.ui" line="389"/>
        <location filename="../rigcreator.ui" line="453"/>
        <location filename="../rigcreator.ui" line="517"/>
        <location filename="../rigcreator.ui" line="573"/>
        <location filename="../rigcreator.ui" line="946"/>
        <location filename="../rigcreator.ui" line="1010"/>
        <location filename="../rigcreator.ui" line="1143"/>
        <source>Num</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="246"/>
        <location filename="../rigcreator.ui" line="399"/>
        <location filename="../rigcreator.ui" line="458"/>
        <location filename="../rigcreator.ui" line="522"/>
        <location filename="../rigcreator.ui" line="583"/>
        <location filename="../rigcreator.ui" line="951"/>
        <location filename="../rigcreator.ui" line="1030"/>
        <location filename="../rigcreator.ui" line="1173"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="257"/>
        <source>Periodic Commands</source>
        <translation>Periodieke commando&apos;s</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="270"/>
        <source>Priority</source>
        <translation>Prioriteit</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="280"/>
        <source>VFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="295"/>
        <source>Main Memory Format</source>
        <translation>Hoofdgeheugenformaat</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Format %&amp;lt;start&amp;gt;.&amp;lt;len&amp;gt;&amp;lt;specifier&amp;gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;specifier from list below:&lt;/p&gt;&lt;p&gt;a = Group&lt;/p&gt;&lt;p&gt;b = Num&lt;/p&gt;&lt;p&gt;c = Scan&lt;/p&gt;&lt;p&gt;d = Scan/Split&lt;/p&gt;&lt;p&gt;e = VFO A		E = VFO B&lt;/p&gt;&lt;p&gt;f = Frequency A 	F = Frequency B&lt;/p&gt;&lt;p&gt;g = Mode A		G = Mode B&lt;/p&gt;&lt;p&gt;h = Filter 		H = Filter B&lt;/p&gt;&lt;p&gt;i = Data 		I = Data B&lt;/p&gt;&lt;p&gt;j = Duplex/Tonemode A	J = Duplex B/Tonemode B&lt;/p&gt;&lt;p&gt;k = Data/Tonemode A	K = Data B/Tonemode B&lt;/p&gt;&lt;p&gt;l = Tonemode A	L = Tonemode B&lt;/p&gt;&lt;p&gt;m = DSQL A 		M = DSQL B&lt;/p&gt;&lt;p&gt;n = Tone type A	N = Tone type B&lt;/p&gt;&lt;p&gt;o = TSQL A		O = TSQL B&lt;/p&gt;&lt;p&gt;p = DTCS Polarity A 	P = DTCS Polarity B&lt;/p&gt;&lt;p&gt;q = DTCS A		Q = DTCS B&lt;/p&gt;&lt;p&gt;r = DV Squelch A 	R = DV Squelch B&lt;/p&gt;&lt;p&gt;s = Duplex Offset A 	S = Duplex Offset B&lt;/p&gt;&lt;p&gt;t = DV UR A		T = DV UR B&lt;/p&gt;&lt;p&gt;u = DV R1 A		U = DV R1 B&lt;/p&gt;&lt;p&gt;v = DV R2 A 		V = DV R2 B&lt;/p&gt;&lt;p&gt;z = Memory Name&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Formaat %&amp;lt;start&amp;gt;.&amp;lt;len&amp;gt;&amp;lt;specificatie&amp;gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;specificatie uit onderstaande lijst :&lt;/p&gt;&lt;p&gt;a = Groep&lt;/p&gt;&lt;p&gt;b = Aantal&lt;/p&gt;&lt;p&gt;c = Scannen&lt;/p&gt;&lt;p&gt;d = Scannen/Splitsen&lt;/p&gt;&lt;p&gt; e = VFO A E = VFO B&lt;/p&gt;&lt;p&gt;f = Frequentie A F = Frequentie B&lt;/p&gt;&lt;p&gt;g = Modus A G = Modus B&lt;/p&gt;&lt;p&gt;h = Filter H = Filter B&lt;/p&gt;&lt;p&gt;i = Data I = Data B&lt;/p&gt;&lt;p&gt;j = Duplex/Toonmodus A J = Duplex B/Toonmodus B&lt;/p&gt;&lt;p&gt;k = Data/Toonmodus A K = Gegevens B/Toonmodus B&lt;/p&gt;&lt;p&gt;l = Toonmodus A L = Toonmodus B&lt;/p&gt;&lt;p&gt;m = DSQL A M = DSQL B&lt;/p&gt;&lt;p&gt;n = Toontype A N = Toontype B&lt;/p&gt;&lt;p&gt;o = TSQL A O = TSQL B&lt;/p&gt;&lt;p&gt;p = DTCS-polariteit A P = DTCS-polariteit B&lt;/p&gt;&lt;p&gt;q = DTCS A Q = DTCS B&lt;/p &gt;&lt;p&gt;r = DV-squelch A R = DV-squelch B&lt;/p&gt;&lt;p&gt;s = Duplex-offset A S = Duplex-offset B&lt;/p&gt;&lt;p&gt;t = DV UR A T = DV UR B&lt;/p&gt; &lt;p&gt;u = DV R1 A U = DV R1 B&lt;/p&gt;&lt;p&gt;v = DV R2 A V = DV R2 B&lt;/p&gt;&lt;p&gt;z = Geheugennaam&lt;/p&gt;&lt;/body&gt;&lt;/ html&gt;</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="308"/>
        <location filename="../rigcreator.ui" line="332"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Format %&amp;lt;start&amp;gt;.&amp;lt;len&amp;gt;&amp;lt;specifier&amp;gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;specifier from list below:&lt;/p&gt;&lt;p&gt;a = Group&lt;/p&gt;&lt;p&gt;b = Num&lt;/p&gt;&lt;p&gt;c = Scan&lt;/p&gt;&lt;p&gt;d = Scan/Split&lt;/p&gt;&lt;p&gt;D = Duplex Setting (use j for most rigs)&lt;/p&gt;&lt;p&gt;e = VFO A E = VFO B&lt;/p&gt;&lt;p&gt;f = Frequency A F = Frequency B&lt;/p&gt;&lt;p&gt;g = Mode A G = Mode B&lt;/p&gt;&lt;p&gt;h = Filter H = Filter B&lt;/p&gt;&lt;p&gt;i = Data I = Data B&lt;/p&gt;&lt;p&gt;j = Duplex/Tonemode A J = Duplex B/Tonemode B&lt;/p&gt;&lt;p&gt;k = Data/Tonemode A K = Data B/Tonemode B&lt;/p&gt;&lt;p&gt;l = Tonemode A L = Tonemode B&lt;/p&gt;&lt;p&gt;m = DSQL A M = DSQL B&lt;/p&gt;&lt;p&gt;n = Tone type A N = Tone type B&lt;/p&gt;&lt;p&gt;o = TSQL A O = TSQL B&lt;/p&gt;&lt;p&gt;p = DTCS Polarity A P = DTCS Polarity B&lt;/p&gt;&lt;p&gt;q = DTCS A Q = DTCS B&lt;/p&gt;&lt;p&gt;r = DV Squelch A R = DV Squelch B&lt;/p&gt;&lt;p&gt;s = Duplex Offset A S = Duplex Offset B&lt;/p&gt;&lt;p&gt;t = DV UR A T = DV UR B&lt;/p&gt;&lt;p&gt;u = DV R1 A U = DV R1 B&lt;/p&gt;&lt;p&gt;v = DV R2 A V = DV R2 B&lt;/p&gt;&lt;p&gt;w = Tuning Step (+custom)&lt;/p&gt;&lt;p&gt;x = Preamp + Attenuator&lt;/p&gt;&lt;p&gt;y = Antenna&lt;/p&gt;&lt;p&gt;+ = IP Plus&lt;/p&gt;&lt;p&gt;z = Memory Name&lt;/p&gt;&lt;p&gt;Z = Mode specific columns&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="319"/>
        <source>Satellite Memory Format</source>
        <translation>Satellietgeheugenformaat</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="343"/>
        <source>Inputs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="394"/>
        <location filename="../rigcreator.ui" line="1015"/>
        <source>Reg</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="410"/>
        <source>Spans</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="463"/>
        <source>Freq</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="474"/>
        <source>Antennas</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="533"/>
        <source>Flters</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="578"/>
        <location filename="../rigcreator.ui" line="967"/>
        <source>Modes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="600"/>
        <source>Features</source>
        <translation>Functies</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="606"/>
        <source>Has Spectrum</source>
        <translation>Heeft spectrum</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="613"/>
        <source>Has LAN</source>
        <translation>Heeft LAN</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="620"/>
        <source>Has Ethernet</source>
        <translation>Heeft Ethernet</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="627"/>
        <source>Has WiFi</source>
        <translation>Heeft WiFi</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="634"/>
        <source>Has Transmit</source>
        <translation>Heeft Zenden</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="641"/>
        <source>HasFDComms</source>
        <translation>Heeft FDComms</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="648"/>
        <source>Has Command 29</source>
        <translation>Heeft commando 29</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="662"/>
        <source>Load File</source>
        <translation>Bestand Laden</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="669"/>
        <source>Save File</source>
        <translation>Bestand Opslaan</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="689"/>
        <source>Default Rigs</source>
        <translation>Standaard Rigs</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="696"/>
        <source>Manufacturer</source>
        <translation>Fabrikant</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="704"/>
        <source>Icom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="709"/>
        <source>Yaesu</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="714"/>
        <source>Kenwood</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="722"/>
        <source>Model</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="732"/>
        <source>C-IV Address</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="742"/>
        <source>RigCtlD Model</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="749"/>
        <source>9999</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="766"/>
        <source>Spectrum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="774"/>
        <source>Seq Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="781"/>
        <source>Len Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="788"/>
        <source>Amp Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="807"/>
        <location filename="../rigcreator.ui" line="826"/>
        <source>000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="851"/>
        <source>00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="858"/>
        <source>Num RX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="871"/>
        <location filename="../rigcreator.ui" line="891"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="878"/>
        <source>VFO per RX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="903"/>
        <source>Tuning Steps</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="956"/>
        <source>Hz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1020"/>
        <source>Min BW</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1025"/>
        <source>Max BW</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1041"/>
        <source>Atten</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1081"/>
        <source>dB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1092"/>
        <source>Bands</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1138"/>
        <source>Region</source>
        <translation>Regio</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1148"/>
        <source>BSR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1158"/>
        <source>End</source>
        <translation>Einde</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1163"/>
        <source>Range</source>
        <translation>Bereik</translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1178"/>
        <source>Bytes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1183"/>
        <source>Pwr (W)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1188"/>
        <source>Ant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1193"/>
        <source>Colour</source>
        <translation>Kleur</translation>
    </message>
    <message>
        <location filename="../rigcreator.cpp" line="123"/>
        <location filename="../rigcreator.cpp" line="126"/>
        <location filename="../rigcreator.cpp" line="149"/>
        <location filename="../rigcreator.cpp" line="151"/>
        <location filename="../rigcreator.cpp" line="583"/>
        <location filename="../rigcreator.cpp" line="585"/>
        <source>Select Rig Filename</source>
        <translation>Selecteer Rig-bestandsnaam</translation>
    </message>
    <message>
        <location filename="../rigcreator.cpp" line="177"/>
        <source>Not a rig definition</source>
        <translation>Geen rig-definitie</translation>
    </message>
    <message>
        <location filename="../rigcreator.cpp" line="178"/>
        <source>File %0 does not appear to be a valid Rig definition file</source>
        <translation>Bestand %0 lijkt geen geldig Rig-definitiebestand te zijn</translation>
    </message>
    <message>
        <location filename="../rigcreator.cpp" line="880"/>
        <source>rig creator</source>
        <translation>rig maker</translation>
    </message>
    <message>
        <location filename="../rigcreator.cpp" line="880"/>
        <source>Changes will be lost!</source>
        <translation>Wijzigingen gaan verloren!</translation>
    </message>
</context>
<context>
    <name>satelliteSetup</name>
    <message>
        <location filename="../satellitesetup.ui" line="32"/>
        <source>Satellite Setup</source>
        <translation>Satelliet Setup</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="101"/>
        <source>Satellite Setup:</source>
        <translation>Satelliet Setup</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="141"/>
        <source>Type:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="148"/>
        <source>Linear Inverting</source>
        <translation>Lineair inverteren</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="155"/>
        <source>Linear Non-Inverting</source>
        <translation>Lineair niet-inverterend</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="162"/>
        <source>FM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="189"/>
        <source>Uplink:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="212"/>
        <source>Downlink:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="258"/>
        <source>Uplink from:</source>
        <translation>Uplink van:</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="281"/>
        <location filename="../satellitesetup.ui" line="356"/>
        <source>To:</source>
        <translation>Naar:</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="333"/>
        <source>Downlink from:</source>
        <translation>Downlnk van:</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="408"/>
        <source>Telemetry: </source>
        <translation>Telemetrie:</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="451"/>
        <source>Additional Spectrum Margin (KHz)</source>
        <translation>Extra spectrummarge (KHz)</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="468"/>
        <source>(added to both sides)</source>
        <translation>(toegevoegd aan beide zijden)</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="495"/>
        <source>Set VFOs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="502"/>
        <source>Set Spectrum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="509"/>
        <source>Add Markers</source>
        <translation>Markers toevoegen</translation>
    </message>
</context>
<context>
    <name>selectRadio</name>
    <message>
        <location filename="../selectradio.ui" line="14"/>
        <source>Select Radio From List</source>
        <translation>Selecteer Radio uit de lijst</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="35"/>
        <source>Cancel</source>
        <translation>Annuleer</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="58"/>
        <source>Rig Name</source>
        <translation>Rig Naam</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="63"/>
        <source>CI-V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="68"/>
        <source>Baud Rate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="73"/>
        <source>Current User</source>
        <translation>Huidige gebruiker</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="78"/>
        <source>User IP Address</source>
        <translation>Gebruikers IP-adres</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="129"/>
        <source>AF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="162"/>
        <source>MOD</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>settingswidget</name>
    <message>
        <location filename="../settingswidget.ui" line="14"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="59"/>
        <source>Radio Connection</source>
        <translation>Radio Connectie</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="65"/>
        <source>Serial (USB)</source>
        <translation>Serieel (USB)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="72"/>
        <source>Network</source>
        <translation>Netwerk</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="94"/>
        <source>CI-V and Model</source>
        <translation>CI-V en Model</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="100"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you are using an older (year 2010) radio, you may need to enable this option to manually specify the CI-V address. This option is also useful for radios that do not have CI-V Transceive enabled and thus will not answer our broadcast query for connected rigs on the CI-V bus.&lt;/p&gt;&lt;p&gt;If you have a modern radio with CI-V Transceive enabled, you should not need to check this box. &lt;/p&gt;&lt;p&gt;You will need to Save Settings and re-launch wfview for this to take effect. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Als u een oudere radio (jaar 2010) gebruikt, moet u mogelijk deze optie inschakelen om het CI-V-adres handmatig op te geven. Deze optie is ook handig voor radio&apos;s waarvoor CI-V Transceive niet is ingeschakeld en beantwoordt dus niet onze uitzendvraag voor aangesloten apparatuur op de CI-V-bus.&lt;/p&gt;&lt;p&gt;Als u een moderne radio met CI-V Transceive ingeschakeld heeft, hoeft u dit vakje niet aan te vinken. &lt;/p&gt;&lt;p&gt;U moet de instellingen opslaan en wfview opnieuw starten om dit van kracht te laten worden. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="103"/>
        <source>Manual Radio CI-V Address:</source>
        <translation>Handmatig Radio CI-V-adres:</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="110"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only check for older radios!&lt;/p&gt;&lt;p&gt;This checkbox forces wfview to trust that the CI-V address is also the model number of the radio. This is only useful for older radios that do not reply to our Rig ID requests (0x19 0x00). Do not check this box unless you have an older radio. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Alleen controleren op oudere radio&apos;s!&lt;/p&gt;&lt;p&gt;Dit selectievakje dwingt wfview erop te vertrouwen dat het CI-V-adres ook het modelnummer van de radio is. Dit is alleen nuttig voor oudere radio&apos;s die niet reageren op onze Rig ID-verzoeken (0x19 0x00). Vink dit vakje alleen aan als u een oudere radio heeft. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="113"/>
        <source>Use CI-V address as Model ID too</source>
        <translation>Gebruik het CI-V-adres ook als model-ID</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter the address in as hexadecimal, without any prefix, just as the radio presents the address in the menu. &lt;/p&gt;&lt;p&gt;Here are some common examples:&lt;/p&gt;
&lt;p&gt;IC-706: 58 
&lt;br/&gt;IC-756: 50
&lt;br/&gt;IC-756 Pro: 5C
&lt;br/&gt;IC-756 Pro II: 64
&lt;br/&gt;IC-756 Pro III: 6E
&lt;br/&gt;IC-7000: 70
&lt;br/&gt;IC-7100: 88
&lt;br/&gt;IC-7200: 76 
&lt;br/&gt;IC-7300: 94
&lt;/p&gt;&lt;p&gt;This setting is typically needed for older radios and for radios that do not have CI-V Transceive enabled. &lt;/p&gt;
&lt;p&gt;After changing, press Save Settings and re-launch wfview.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voer het adres in als hexadecimaal, zonder voorvoegsel, net zoals de radio het adres in het menu presenteert. &lt;/p&gt;&lt;p&gt;Hier zijn enkele veelvoorkomende voorbeelden:&lt;/p&gt;
&lt;p&gt;IC-706: 58
&lt;br/&gt;IC-756: 50
&lt;br/&gt;IC-756 Pro: 5C
&lt;br/&gt;IC-756 Pro II: 64
&lt;br/&gt;IC-756 Pro III: 6E
&lt;br/&gt;IC-7000: 70
&lt;br/&gt;IC-7100: 88
&lt;br/&gt;IC-7200: 76
&lt;br/&gt;IC-7300: 94
&lt;/p&gt;&lt;p&gt;Deze instelling is doorgaans nodig voor oudere radio&apos;s en voor radio&apos;s waarvoor CI-V Transceive niet is ingeschakeld. &lt;/p&gt;
&lt;p&gt;Na het wijzigen drukt u op Instellingen opslaan en start u wfview opnieuw.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="149"/>
        <source>auto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="165"/>
        <source>Information</source>
        <translation>Informatie</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="176"/>
        <source>Audio controls on this page are ONLY for network radios
Please use the &quot;Radio Server&quot; page to select server audio.
ONLY use Manual CI-V when Transceive mode is not supported</source>
        <translation>Audiobedieningen op deze pagina zijn ALLEEN voor netwerkradio&apos;s
Gebruik de pagina &quot;Radioserver&quot; om serveraudio te selecteren.
Gebruik ALLEEN handmatige CI-V als de zendontvangermodus niet wordt ondersteund</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="203"/>
        <source>Serial Connected Radios</source>
        <translation>Serieel aangesloten radio&apos;s</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="221"/>
        <source>Serial Device:</source>
        <translation>Serieel apparaat:</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="244"/>
        <source>Baud Rate</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This feature is for older radios that respond best to an RTS serial port signal than a PTT command.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;For radios lacking PTT commands, this is automatic and transparent, but for radios which have a PTT command, you can check this box to override and force the PTT to be done using RTS. Do not check this box unless you really need this and have an appropriate adapter with RTS connected to the PTT line of the transceiver. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deze functie is bedoeld voor oudere radio&apos;s die het beste reageren op een RTS-seriële poortsignaal i.p.v. op een PTT-opdracht.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;Voor radio&apos;s zonder PTT-opdrachten, dit is automatisch en transparant, maar voor radio&apos;s die een PTT-commando hebben, kunt u dit vakje aanvinken om de PTT te overschrijven en te forceren via RTS. Vink dit vakje alleen aan als je dit echt nodig hebt en een geschikte adapter met RTS hebt aangesloten op de PTT-lijn van de transceiver. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Send RTS for PTT</source>
        <translation type="vanished">Stuur RTS voor PTT</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="316"/>
        <source>Network Connected Radios</source>
        <translation>Netwerk aangesloten radio&apos;s</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="348"/>
        <source>Hostname</source>
        <translation>Hostnaam</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="371"/>
        <location filename="../settingswidget.ui" line="2602"/>
        <source>Control Port</source>
        <translation>Control Poort</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="384"/>
        <location filename="../settingswidget.ui" line="2630"/>
        <source>50001</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="417"/>
        <location filename="../settingswidget.ui" line="2895"/>
        <location filename="../settingswidget.ui" line="3371"/>
        <source>Username</source>
        <translation>Gebruikersnaam</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="440"/>
        <location filename="../settingswidget.ui" line="2900"/>
        <location filename="../settingswidget.ui" line="3381"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="483"/>
        <source>RX Latency (ms)</source>
        <translation>RX-vertraging (ms)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="509"/>
        <location filename="../settingswidget.ui" line="536"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="516"/>
        <source>TX Latency (ms)</source>
        <translation>TX-vertraging (ms)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="543"/>
        <source>RX Codec</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="550"/>
        <source>Receive Audio Codec Selector</source>
        <translation>Ontvang Audio Codec Kiezer</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="557"/>
        <source>TX Codec</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="564"/>
        <source>Transmit Audio Codec Selector</source>
        <translation>Zend Audio Codec Kiezer</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="588"/>
        <source>Sample Rate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="595"/>
        <source>Audio Sample Rate Selector</source>
        <translation>Audiosamplefrequentie Kiezer</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="599"/>
        <source>48000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="604"/>
        <source>24000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="609"/>
        <source>16000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="614"/>
        <source>8000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="622"/>
        <source>Duplex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="630"/>
        <source>Full Duplex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="635"/>
        <source>Half Duplex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="643"/>
        <location filename="../settingswidget.ui" line="2804"/>
        <source>Audio System</source>
        <translation>Audio Systeem</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="651"/>
        <location filename="../settingswidget.ui" line="2812"/>
        <source>Qt Audio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="656"/>
        <location filename="../settingswidget.ui" line="2817"/>
        <source>PortAudio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="661"/>
        <location filename="../settingswidget.ui" line="2822"/>
        <source>RT Audio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="666"/>
        <source>TCI Audio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="691"/>
        <source>Audio Output </source>
        <translation>Audio Uitgang</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="704"/>
        <source>Audio Output Selector</source>
        <translation>Audio Uitgang Kiezer</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="711"/>
        <source>Audio Input</source>
        <translation>Audio Ingang</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="724"/>
        <source>Audio Input Selector</source>
        <translation>Audio Ingang Kiezer</translation>
    </message>
    <message>
        <source>Connect To Radio</source>
        <translation type="vanished">Verbinding maken met radio</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="783"/>
        <source>When tuning, set lower digits to zero</source>
        <translation>Zet bij het afstemmen de laagste cijfers op nul</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="793"/>
        <location filename="../settingswidget.ui" line="799"/>
        <source>When using SSB, automatically switch to the standard sideband for a given band.</source>
        <translation>Wanneer u SSB gebruikt, schakelt u automatisch over naar de standaardzijband voor een bepaalde band.</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="796"/>
        <source>Auto SSB Switching</source>
        <translation>Auto SSB-schakeling</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="802"/>
        <source>Auto SSB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="809"/>
        <source>Enable PTT Controls</source>
        <translation>PTT-bediening inschakelen</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="816"/>
        <source>Rig creator allows changing of all rig features and adding new rig profiles</source>
        <translation>Met Rig Creator kunnen alle rig-functies worden gewijzigd en nieuwe rig-profielen worden toegevoegd</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="819"/>
        <source>Enable Rig Creator Feature (use with care)</source>
        <translation>Rig Creator-functie inschakelen (Let Op!)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="842"/>
        <source>Region:</source>
        <translation>Regio:</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="861"/>
        <location filename="../settingswidget.ui" line="1282"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="872"/>
        <source>Enables interpolation between pixels. Note that this will increase CPU usage.</source>
        <translation>Maakt interpolatie tussen pixels mogelijk. Houd er rekening mee dat dit het CPU-gebruik zal verhogen.</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="875"/>
        <source>Interpolate Waterfall</source>
        <translation>Waterval interpoleren</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="885"/>
        <source>Anti-Alias Waterfall</source>
        <translation>Anti-alias-waterval</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="892"/>
        <source>Allow tuning via click and drag (experimental)</source>
        <translation>Afstemming via klikken en slepen toestaan ​​(experimenteel)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="899"/>
        <source>Use System Theme</source>
        <translation>Gebruik Systeemthema</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="906"/>
        <source>Show full screen (F11)</source>
        <translation>Volledig scherm weergeven (F11)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="930"/>
        <source>Frequency Display:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="937"/>
        <source>Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="973"/>
        <source>Separators:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="980"/>
        <source>Decimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="990"/>
        <source>Groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1029"/>
        <source>Underlay Mode</source>
        <translation>Underlay Modus</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1036"/>
        <source>No underlay graphics</source>
        <translation>Geen underlay afbeeldingen</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="945"/>
        <location filename="../settingswidget.ui" line="1039"/>
        <source>None</source>
        <translation>Geen</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1049"/>
        <source>Indefinite peak hold</source>
        <translation>Onbepaalde piek hold</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1052"/>
        <source>Peak Hold</source>
        <translation>Piek Hold</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1059"/>
        <source>Peak value within the buffer</source>
        <translation>Piekwaarde binnen de buffer</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1062"/>
        <source>Peak</source>
        <translation>Piek</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1069"/>
        <source>Average value within the buffer</source>
        <translation>Gemiddelde waarde binnen de buffer</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1072"/>
        <source>Average</source>
        <translation>Gemiddeld</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1079"/>
        <source>Underlay Buffer Size:</source>
        <translation>Underlay Buffer Grootte</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1092"/>
        <source>Size of buffer for spectrum data. Shorter values are more responsive.</source>
        <translation>Grootte van de buffer voor spectrumgegevens. Kortere waarden reageren beter.</translation>
    </message>
    <message>
        <source>Frequency Units</source>
        <translation type="vanished">Frequentie-eenheden</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="950"/>
        <source>Hz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="960"/>
        <source>MHz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="965"/>
        <source>GHz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1111"/>
        <source>Show Bands</source>
        <translation>Toon Banden</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1151"/>
        <source>Additional Meter Selection:</source>
        <translation>Extra meterselectie:</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1164"/>
        <source>Broadcast-style reduction meter</source>
        <translation>Reductiemeter in Omroepstijl</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1167"/>
        <source>Reverse Comp Meter</source>
        <translation>Omgekeerde Comp Meter</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1174"/>
        <source>wfview will automatically calculate command polling. Recommended.</source>
        <translation>wfview berekent automatisch de opdrachtpolling. Aanbevolen.</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1177"/>
        <source>AutoPolling</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1190"/>
        <source>Manual (user-defined) command polling</source>
        <translation>Handmatige (door de gebruiker gedefinieerde) opdrachtopvraging</translation>
    </message>
    <message>
        <source>Manual Polling Inteval:</source>
        <translation type="vanished">Handmatig pollinginterval:</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1193"/>
        <source>Manual Polling Interval:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1203"/>
        <source>Sets the polling interval, in ms. Automatic polling is recommended. Serial port and USB port radios should not poll quicker than about 75ms.</source>
        <translation>Stelt het polling-interval in ms in. Automatisch pollen wordt aanbevolen. Seriële poort- en USB-poortradio&apos;s mogen niet sneller dan ongeveer 75 ms pollen.</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1219"/>
        <source>ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1249"/>
        <source>Color scheme</source>
        <translation>Kleuren Schema</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1256"/>
        <source>Preset:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1278"/>
        <source>Select a color preset here.</source>
        <translation>Kies hier een kleuren preset</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1287"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1292"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1297"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1302"/>
        <source>5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1310"/>
        <source>Revert the selected color preset to the default.</source>
        <translation>Zet de geselecteerde kleuren preset terug naar de standaardwaarde.</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1313"/>
        <source>Revert</source>
        <translation>Terugdraaien</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1320"/>
        <source>Rename the selected color preset. Max length is 10 characters.</source>
        <translation>Hernoem de geselecteerde kleuren preset. De maximale lengte is 10 tekens.</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1323"/>
        <location filename="../settingswidget.cpp" line="2393"/>
        <source>Rename Preset</source>
        <translation>Hernoem Preset</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1363"/>
        <source>User-defined Color Editor</source>
        <translation>Door de gebruiker gedefinieerde kleureneditor</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1460"/>
        <location filename="../settingswidget.ui" line="1493"/>
        <location filename="../settingswidget.ui" line="1537"/>
        <location filename="../settingswidget.ui" line="1617"/>
        <location filename="../settingswidget.ui" line="1702"/>
        <location filename="../settingswidget.ui" line="1732"/>
        <location filename="../settingswidget.ui" line="1752"/>
        <location filename="../settingswidget.ui" line="1775"/>
        <location filename="../settingswidget.ui" line="1805"/>
        <location filename="../settingswidget.ui" line="1842"/>
        <location filename="../settingswidget.ui" line="1862"/>
        <location filename="../settingswidget.ui" line="1882"/>
        <location filename="../settingswidget.ui" line="1901"/>
        <location filename="../settingswidget.ui" line="1914"/>
        <location filename="../settingswidget.ui" line="1934"/>
        <location filename="../settingswidget.ui" line="2015"/>
        <location filename="../settingswidget.ui" line="2028"/>
        <location filename="../settingswidget.ui" line="2041"/>
        <location filename="../settingswidget.ui" line="2071"/>
        <location filename="../settingswidget.ui" line="2114"/>
        <location filename="../settingswidget.ui" line="2127"/>
        <location filename="../settingswidget.ui" line="2170"/>
        <location filename="../settingswidget.ui" line="2183"/>
        <location filename="../settingswidget.ui" line="2210"/>
        <location filename="../settingswidget.ui" line="2230"/>
        <location filename="../settingswidget.ui" line="2276"/>
        <location filename="../settingswidget.ui" line="2289"/>
        <location filename="../settingswidget.ui" line="2302"/>
        <source>#AARRGGBB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1662"/>
        <source>Spectrum Line</source>
        <translation>Spectrumlijn</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2002"/>
        <source>Axis</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1638"/>
        <source>Grid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2111"/>
        <source>Color text format is #AARRGGBB, where AA is the &quot;alpha&quot; channel, and value &quot;00&quot; is totally transparent, and &quot;ff&quot; is totally opaque.</source>
        <translation>Het kleurentekstformaat is #AARRGGBB, waarbij AA het &quot;alfa&quot;-kanaal is, en de waarde &quot;00&quot; volledig transparant is, en &quot;ff&quot; volledig ondoorzichtig.</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1829"/>
        <source>Meter Peak Level</source>
        <translation>Meterpiekniveau</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1500"/>
        <source>Underlay Line</source>
        <translation>Underlay Lijn</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1921"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2197"/>
        <source>Spectrum Fill Top</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1965"/>
        <source>Plot Background</source>
        <translation>Plot achtergrond</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1941"/>
        <source>Spectrum Gradient</source>
        <translation>Spectrumgradiënt</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1631"/>
        <source>Underlay Fill</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1948"/>
        <source>Meter Average</source>
        <translation>Meter Gemiddeld</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1849"/>
        <source>Passband</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1447"/>
        <source>Waterfall Grid</source>
        <translation>Watervalraster</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1624"/>
        <source>Waterfall Back</source>
        <translation>Waterval achtergrond</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1869"/>
        <source>Tuning Line</source>
        <translation>Afstemlijn</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1655"/>
        <source>Waterfall Axis</source>
        <translation>Waterval-as</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1709"/>
        <source>Waterfall Text</source>
        <translation>Waterval tekst</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1739"/>
        <source>Meter Level</source>
        <translation>Meterniveau</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2217"/>
        <source>Meter Text</source>
        <translation>Metertekst</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2147"/>
        <source>Spectrum Fill Bot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1564"/>
        <source>Spectrum Fill</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1782"/>
        <source>PBT Indicator</source>
        <translation>PBT-indicator</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2190"/>
        <source>Meter Scale</source>
        <translation>Meter Schaal</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1689"/>
        <source>Meter High Scale</source>
        <translation>Meter hoge schaal</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="261"/>
        <source>PTT Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="269"/>
        <source>CI-V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="274"/>
        <source>RTS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="279"/>
        <source>DTR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="751"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;You &lt;span style=&quot; font-weight:700;&quot;&gt;MUST&lt;/span&gt; disconnect from the radio before making any changes in the above form.&lt;br/&gt;&lt;br/&gt;Please use the &lt;span style=&quot; font-style:italic;&quot;&gt;Connect/Disconnect &lt;/span&gt;button below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="839"/>
        <location filename="../settingswidget.ui" line="855"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ITU Region. Used to display band limits. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Region 1&lt;/span&gt; comprises Europe, Africa, the Commonwealth of Independent States, Mongolia, and the Middle East west of the Persian Gulf, including Iraq.&lt;/p&gt;&lt;p&gt;The western boundary is defined by Line B.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Region 2&lt;/span&gt; covers the Americas including Greenland, and some of the eastern Pacific Islands.&lt;/p&gt;&lt;p&gt;The eastern boundary is defined by Line B.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Region 3&lt;/span&gt; contains most of non-FSU Asia east of and including Iran, and most of Oceania.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Line B&lt;/span&gt; is a line running from the North Pole along meridian 10° West of Greenwich to its intersection with parallel 72° North; thence by great circle arc to the intersection of meridian 50° West and parallel 40° North; thence by great circle arc to the intersection of meridian 20° West and parallel 10° South; thence along meridian 20° West to the South Pole.&lt;/p&gt;&lt;p&gt;(Text is from the English wikipedia)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="955"/>
        <source>kHz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1430"/>
        <source>Cluster Spots</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1517"/>
        <source>Underlay Gradient</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1524"/>
        <source>Underlay Fill Top</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1822"/>
        <source>Button On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2058"/>
        <source>Button Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2088"/>
        <source>Underlay Fill Bot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2337"/>
        <source>Data Off Modulation Input:</source>
        <translation>Data Off modulatie-ingang:</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2344"/>
        <source>Modulation Input</source>
        <translation>Modulatie Ingang</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2347"/>
        <source>Transmit modulation source</source>
        <translation>Zend modulatie bron</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2357"/>
        <source>(Data Mod Inputs) DATA1:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2364"/>
        <source>Data Modulation Input</source>
        <translation>Data Modulatie Ingang</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2367"/>
        <source>Transmit Data-mode modulation input source</source>
        <translation>Zend Data Mode modulatie ingangsbron</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2377"/>
        <source>DATA2:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2387"/>
        <source>DATA3:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2414"/>
        <source>Press here to set the clock of the radio. The command will be sent to the radio when the seconds go to zero. </source>
        <translation>Druk hier om de klok van de radio in te stellen. Het commando wordt naar de radio verzonden als de seconden naar nul gaan.</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2417"/>
        <source>Set Clock</source>
        <translation>Klok instellen</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2424"/>
        <source>Check this box to set the radio&apos;s clock to UTC. Otherwise, the clock will be set to the local timezone of this computer. </source>
        <translation>Vink dit vakje aan om de klok van de radio in te stellen op UTC. Anders wordt de klok ingesteld op de lokale tijdzone van deze computer.</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2427"/>
        <source>Use UTC</source>
        <translation>Gebruik UTC</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2434"/>
        <source>Set radio time on connect (takes up to a minute)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2454"/>
        <source>Satellite Ops</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2461"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Click here to adjust the frequency reference on the IC-9700.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Klik hier om de frequentiereferentie op de IC-9700 aan te passen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2464"/>
        <source>Adjust Reference</source>
        <translation>Referentie aanpassen</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2478"/>
        <source>Manual PTT Toggle</source>
        <translation>Handmatige PTT-schakelaar</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2485"/>
        <source>PTT On</source>
        <translation>PTT Aan</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2488"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2495"/>
        <source>PTT Off</source>
        <translation>PTT Uit</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2551"/>
        <source>Enable</source>
        <translation>Inschakelen</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2571"/>
        <source>Disable local user controls when in use (restart required)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2582"/>
        <source>Server Setup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2627"/>
        <location filename="../settingswidget.ui" line="2677"/>
        <location filename="../settingswidget.ui" line="2718"/>
        <source>99999</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2658"/>
        <source>Civ Port</source>
        <translation>Civ-poort</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2680"/>
        <source>50002</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2699"/>
        <source>Audio Port</source>
        <translation>Audio-poort</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2721"/>
        <source>50003</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2745"/>
        <source>RX Audio Input</source>
        <translation>RX-audio bron</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2768"/>
        <source>TX Audio Output</source>
        <translation>TX-audio bron</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2834"/>
        <source>Users</source>
        <translation>Gebruikers</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2905"/>
        <source>Admin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2920"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:700;&quot;&gt;Please disconnect from radio to make changes to the server settings&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2965"/>
        <source>Enable RigCtld</source>
        <translation>RigCtld inschakelen</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2988"/>
        <source>Port</source>
        <translation>Poort</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3028"/>
        <source>Virtual Serial Port</source>
        <translation>Virtuele seriële poort</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3047"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this to define a virtual serial port. &lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;On Windows, the virtual serial port can be used to connect to a serial port loopback device, through which other programs can connect to the radio. &lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;On Linux and macOS, the port defined here is a pseudo-terminal device, which may be connected to directly by any program designed for a serial connection. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Gebruik dit om een ​​virtuele seriële poort te definiëren. &lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;In Windows kan de virtuele seriële poort worden gebruikt om verbinding te maken met een loopback-apparaat met een seriële poort, waardoor andere programma&apos;s verbinding kunnen maken met de radio. &lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;Op Linux en macOS is de hier gedefinieerde poort een pseudo-terminalapparaat, waarmee rechtstreeks verbinding kan worden gemaakt door elk programma dat is ontworpen voor een seriële verbinding. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3050"/>
        <source>Virtual Serial Port Selector</source>
        <translation>Virtuele seriële poort kiezer</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3074"/>
        <source>TCP Server Port</source>
        <translation>TCP server poort</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3094"/>
        <source>Enter port for TCP server, 0 = disabled (restart required if changed)</source>
        <translation>Voer poort voor TCP-server in, 0 = uitgeschakeld (herstart vereist indien gewijzigd)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3118"/>
        <source>TCI Server Port</source>
        <translation>TCI server poort</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3135"/>
        <source>Enter port for TCI server 0 = disabled (restart required if changed)</source>
        <translation>Poort voor TCI-server invoeren 0 = uitgeschakeld (herstart vereist indien gewijzigd)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3159"/>
        <source>Waterfall Format</source>
        <translation>Waterval formaat</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3167"/>
        <source>Default</source>
        <translation>Standaard</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3172"/>
        <source>Single (network)</source>
        <translation>Enkel (netwerk)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3177"/>
        <source>Multi (serial)</source>
        <translation>Multi (serieel)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3185"/>
        <source>Only change this if you are absolutely sure you need it (connecting to N1MM+ or similar)</source>
        <translation>Wijzig dit alleen als u er absoluut zeker van bent dat u het nodig heeft (verbinden met N1MM+ of iets dergelijks)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3209"/>
        <source>Enable USB Controllers</source>
        <translation>Schakel USB-controllers in</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3219"/>
        <source>Setup USB Controller</source>
        <translation>USB-controller instellen</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3239"/>
        <source>Reset Buttons</source>
        <translation>Reset Knoppen</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3246"/>
        <source>Only reset buttons/commands if you have issues. </source>
        <translation>Reset knoppen/opdrachten alleen als u problemen ondervindt.</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3290"/>
        <source>This page contains configuration for DX Cluster, either UDP style broadcast (from N1MM+/DXlog) or TCP connection to your favourite cluster</source>
        <translation>Deze pagina bevat configuratie voor DX Cluster, uitzending in UDP-stijl (van N1MM+/DXlog) of TCP-verbinding met uw favoriete cluster</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3299"/>
        <source>TCP Cluster Connection</source>
        <translation>TCP cluster connectie</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3310"/>
        <source>Server Name</source>
        <translation>Server naam</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3335"/>
        <source>Add/Update</source>
        <translation>Toevoegen/Updaten</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3348"/>
        <source>Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3357"/>
        <source>Server Port</source>
        <translation>Server Poort</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3364"/>
        <location filename="../settingswidget.ui" line="3466"/>
        <source>00000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3395"/>
        <source>Spot Timeout (minutes)</source>
        <translation>Spot Timeout (minuten)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3402"/>
        <source>0000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3414"/>
        <source>Connect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3421"/>
        <source>Disconnect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3448"/>
        <source>UDP Broadcast Connection</source>
        <translation>UDP Broadcast Connectie</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3459"/>
        <source>UDP Port</source>
        <translation>UDP Poort</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3490"/>
        <source>Show Skimmer Spots</source>
        <translation>Toon Skimmer Spots</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3521"/>
        <source>This page contains experimental features. Use at your own risk.</source>
        <translation>Deze pagina bevat experimentele functies. Gebruik op eigen risico.</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3532"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This button runs debug functions, and is provided as a convenience for programmers. The functions executed are under:&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; color:#ffff55;&quot;&gt;void&lt;/span&gt;&lt;span style=&quot; color:#55ff55;&quot;&gt;wfmain&lt;/span&gt;&lt;span style=&quot; color:#aaaaaa;&quot;&gt;::&lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;on_debugBtn_clicked&lt;/span&gt;&lt;span style=&quot; color:#aaaaaa;&quot;&gt;()&lt;/span&gt;&lt;/p&gt;&lt;p&gt;in wfmain.cpp.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deze knop voert debug-functies uit en is bedoeld voor het gemak van programmeurs. De uitgevoerde functies staan ​​onder:&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; color:#ffff55;&quot;&gt;void&lt;/span&gt;&lt;span style=&quot; color:#55ff55;&quot;&gt;wfmain&lt;/span&gt;&lt;span style =&quot; color:#aaaaaa;&quot;&gt;::&lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;op_debugBtn_clicked&lt;/span&gt;&lt;span style=&quot; color:#aaaaaa;&quot;&gt;()&lt;/span &gt;&lt;/p&gt;&lt;p&gt;in wfmain.cpp.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3535"/>
        <source>Debug</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3538"/>
        <source>Ctrl+Alt+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3609"/>
        <source>Save Settings</source>
        <translation>Instellingen opslaan</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3616"/>
        <source>Revert to Default</source>
        <translation>Terug naar standaard</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3636"/>
        <source>Connect to Radio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="46"/>
        <source>Radio Access</source>
        <translation>Radio Toegang</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="47"/>
        <source>User Interface</source>
        <translation>Gebruikersomgeving</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="48"/>
        <source>Radio Settings</source>
        <translation>Radio Instellingen</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="49"/>
        <source>Radio Server</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="50"/>
        <source>External Control</source>
        <translation>Externe Toegang</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="51"/>
        <source>DX Cluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="52"/>
        <source>Experimental</source>
        <translation>Experimenteel</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1054"/>
        <source>
Server audio output device does not exist, please check.
Transmit audio will NOT work until this is corrected
**** please disable the server if not required ****</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1065"/>
        <source>
Server audio input device does not exist, please check.
Receive audio will NOT work until this is corrected
**** please disable the server if not required ****</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1353"/>
        <source>Admin User</source>
        <translation>Admin Gebruiker</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1353"/>
        <source>Normal User</source>
        <translation>Normale Gebruiker</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1353"/>
        <source>Normal with no TX</source>
        <comment>Monitor only</comment>
        <translation>Normaal zonder TX</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1361"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1623"/>
        <source>Manual port assignment</source>
        <translation>Handmatige poorttoewijzing</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1624"/>
        <source>Enter serial port assignment:</source>
        <translation>Voer seriële poorttoewijzing in:</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1626"/>
        <source>/dev/device</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="2229"/>
        <source>Specify Opacity</source>
        <translation>Geef ondoorzichtigheid op</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="2230"/>
        <source>You specified an opacity value of 0. 
Do you want to change it? (0=transparent, 255=opaque)</source>
        <translation>U heeft een ondoorzichtigheidswaarde van 0 opgegeven.
Wil je het veranderen? (0=transparant, 255=ondoorzichtig)</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="2394"/>
        <source>Preset Name (10 characters max):</source>
        <translation>Voorkeuzenaam (max. 10 tekens):</translation>
    </message>
</context>
<context>
    <name>wfmain</name>
    <message>
        <location filename="../wfmain.ui" line="17"/>
        <source>wfmain</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="197"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Turns the radio on&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zet de radio aan&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="200"/>
        <source>Power On</source>
        <translation>Aanzetten</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="210"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Turns the radio off&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zet de radio uit&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="213"/>
        <source>Power Off</source>
        <translation>Uitschakelen</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="247"/>
        <source>Tuning Dial</source>
        <translation>Afstemknop</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="272"/>
        <source>Tuning Step Selection possibly. Or not...</source>
        <translation>Tuning Step Selectie mogelijk. Of niet...</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="275"/>
        <source>Tuning Step Selection</source>
        <translation>Selectie van afstemmingsstappen</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="285"/>
        <source>Frequency Lock</source>
        <translation>Frequentievergrendeling</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="288"/>
        <source>F Lock</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="303"/>
        <source>R I T Dial</source>
        <translation>R I T Afstemming</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="331"/>
        <source>R I T Enable</source>
        <translation>R I T inschakelen</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="334"/>
        <source>RIT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="400"/>
        <source>RX RF Gain</source>
        <translation>RX RF-versterking</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="403"/>
        <source>RF Gain</source>
        <translation>RF-versterking</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="406"/>
        <source>Receiver RF Gain</source>
        <translation>Ontvanger RF-versterking</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="431"/>
        <source>RF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="463"/>
        <source>RX AF Gain</source>
        <translation>RX AF-versterking</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="466"/>
        <source>AF Gain</source>
        <translation>AF-versterking</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="469"/>
        <source>Receive Audio Level. Sets rig volume on USB rigs, and sets PC volume on LAN rigs.</source>
        <translation>Ontvang audioniveau. Stelt het rig-volume in op USB-rigs en stelt het pc-volume in op LAN-rigs.</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="488"/>
        <source>AF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="520"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Squelch&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="523"/>
        <source>Squelch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="526"/>
        <source>Squelch control. Top is fully-muted, bottom is wide open.</source>
        <translation>Squelch-controle. De bovenkant is volledig gedempt, de onderkant is wijd open.</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="545"/>
        <source>SQ</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="577"/>
        <source>Mic Gain</source>
        <translation>Microfoonvoorversterking</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="580"/>
        <source>Transmit Audio Gain</source>
        <translation>Zend audioversterking</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="583"/>
        <source>Sets the gain for the transmit audio source, for example mic gain or accessory port gain</source>
        <translation>Stelt de versterking in voor de verzonden audiobron, bijvoorbeeld microfoonversterking of accessoirepoortversterking</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="602"/>
        <source>Mic</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="634"/>
        <location filename="../wfmain.ui" line="637"/>
        <source>Transmit Power</source>
        <translation>Zendvermogen</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="640"/>
        <source>Transmit power level</source>
        <translation>Zendvermorgensniveau</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="659"/>
        <source>TX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="688"/>
        <source>Set the rado monitor level</source>
        <translation>Stel het radiomonitorniveau in</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="710"/>
        <source>&lt;a href=&apos;#&apos;&gt;Mon&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="756"/>
        <source>Noise Blanker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="759"/>
        <source>NB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="766"/>
        <source>Noise Reduction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="769"/>
        <source>NR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="776"/>
        <source>IP+ Funcion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="779"/>
        <source>IP+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="793"/>
        <source>Digi-Sel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="796"/>
        <source>DS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="803"/>
        <source>Compressor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="806"/>
        <source>CMP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="813"/>
        <source>Vox Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="816"/>
        <source>VOX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="847"/>
        <source>Transmit and Receive button</source>
        <translation>Knop Verzenden en ontvangen</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="850"/>
        <source>Transmit</source>
        <translation>Zenden</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="857"/>
        <source>Enable the Automatic Antenna Tuner</source>
        <translation>Schakel de automatische antennetuner in</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="860"/>
        <location filename="../wfmain.ui" line="866"/>
        <source>Enable ATU</source>
        <translation>ATU inschakelen</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="863"/>
        <source>Enable or disable the automatic antenna tuner</source>
        <translation>Schakel de automatische antennetuner in of uit</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="876"/>
        <source>Start the automatic antenna tuner cycle</source>
        <translation>Start de automatische antennetunercyclus</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="879"/>
        <source>Tune</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="889"/>
        <source>Press to bring up the CW Sender</source>
        <translation>Druk op om de CW-zender weer te geven</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="892"/>
        <source>CW</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="902"/>
        <source>Show the repeater tone and offset window</source>
        <translation>Toon de repeatertoon en het offsetvenster</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="905"/>
        <source>Rpt/Split</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="915"/>
        <source>Memories</source>
        <translation>Geheugens</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="941"/>
        <source>Scope Settings</source>
        <translation>Scope Instellingen</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1016"/>
        <source>Main/Sub</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="972"/>
        <source>Dual Watch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="997"/>
        <source>Dual Scope</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1029"/>
        <source>Split</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1042"/>
        <source>Main&lt;&gt;Sub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1052"/>
        <source>Main=Sub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1066"/>
        <source>Preamp/Att</source>
        <translation>Voorversterker/Verzwakker</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1089"/>
        <source>Preamp:</source>
        <translation>Voorversterker</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1096"/>
        <source>Preamp selector</source>
        <translation>Voorversterker kiezer</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1116"/>
        <source>Attenuator:</source>
        <translation>Verzwakker</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1123"/>
        <source>Attenuator selector</source>
        <translation>Verzwakker kiezer</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1138"/>
        <source>Antenna</source>
        <translation>Antenne</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1159"/>
        <source>Antenna port selector</source>
        <translation>Antenne poort kiezer</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1172"/>
        <source>RX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1229"/>
        <source>About</source>
        <translation>Over</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1239"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1249"/>
        <source>Save Settings</source>
        <translation>Instellingen opslaan</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1259"/>
        <source>Radio Status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1269"/>
        <source>Log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1279"/>
        <source>Bands</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1289"/>
        <source>Frequency</source>
        <translation>Frequentie</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1299"/>
        <source>Rig Creator</source>
        <translation>Rig Maker</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1322"/>
        <source>Connect to Radio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1350"/>
        <source> Exit Program</source>
        <translation>Sluit af</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="2819"/>
        <source>wfview</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="2820"/>
        <source>Are you sure you wish to reset the USB controllers?</source>
        <translation>Weet u zeker dat u de USB-controllers wilt resetten?</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4106"/>
        <location filename="../wfmain.cpp" line="4637"/>
        <location filename="../wfmain.cpp" line="5147"/>
        <source>Don&apos;t ask me again</source>
        <translation>Vraag niet nog een keer</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4107"/>
        <source>Don&apos;t ask me to confirm exit again</source>
        <translation>Vraag niet om het afsluiten opnieuw te bevestigen</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4109"/>
        <source>Are you sure you wish to exit?
</source>
        <translation>Weet u zeker dat u wilt afsluiten?</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4200"/>
        <source>Revert settings</source>
        <translation>Instellingen terugzetten</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4201"/>
        <source>Are you sure you wish to reset all wfview settings?
If so, wfview will exit and you will need to start the program again.</source>
        <translation>Weet u zeker dat u alle wfview-instellingen wilt resetten?
Als dit het geval is, wordt wfview afgesloten en moet u het programma opnieuw starten.</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4639"/>
        <source>Power</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4640"/>
        <source>Power down the radio?
</source>
        <translation>Zet de radio uit?</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="5148"/>
        <source>Don&apos;t ask me to confirm memories again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="5150"/>
        <source>Memories are considered an experimental feature,
Please make sure you have a full backup of your radio before making changes.
Are you sure you want to continue?
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
