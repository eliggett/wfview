<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>FirstTimeSetup</name>
    <message>
        <location filename="../firsttimesetup.ui" line="20"/>
        <source>First Time Setup</source>
        <translation>首次安装</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="37"/>
        <source>Welcome to wfview!</source>
        <translation>欢迎使用 wfview</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="53"/>
        <source>How is your radio connected?</source>
        <translation>如何联接您的无线电设备(电台)?</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="59"/>
        <source>Serial Port on this PC</source>
        <translation>使用当前PC的串口</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="69"/>
        <source>USB Port on This PC</source>
        <translation>使用当前PC的USB接口</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="79"/>
        <source>Ethernet Network</source>
        <translation>使用有线网络连接远程设备</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="89"/>
        <source>WiFi Network</source>
        <translation>使用WiFi网络连接远程设备</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="111"/>
        <source>Next Steps:</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="117"/>
        <source>You will now be taken to the Radio Access page under Settings.</source>
        <translation>现在将带您进入设置中的无线电设备访问页面。</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="124"/>
        <source>Please fill in the relevant details for this connection type:</source>
        <translation>请填写此连接类型的相关详细信息：</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="163"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="170"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="177"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="188"/>
        <source>Note: If you do not have this information, press Exit Program, and return later.</source>
        <translation>注意：如果您没有这些信息，请点击退出程序，稍后再回来设置。</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="204"/>
        <source>Even if you have run wfview before, please take a moment to review your settings.</source>
        <translation>即使您之前运行过 wfview，也请花点时间检查您的设置。</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="213"/>
        <source>Press to exit the program.
You will see this dialog box the next time you open wfview.</source>
        <translation>点击退出程序。
下次打开 wfview 时您将再次看到此对话框。</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="217"/>
        <source>Exit Program</source>
        <translation>退出程序</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="224"/>
        <source>Press to skip the setup.</source>
        <translation>点击跳过设置。</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="227"/>
        <source>Skip</source>
        <translation>跳过</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="234"/>
        <source>Press to go back to the prior step. </source>
        <translation>点击返回上一步。</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="237"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="244"/>
        <source>Press for the next step.</source>
        <translation>点击进入下一步。</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.ui" line="247"/>
        <location filename="../firsttimesetup.cpp" line="104"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="17"/>
        <source>Serial Port Name</source>
        <translation>串口名称</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="18"/>
        <source>Baud Rate</source>
        <translation>波特率</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="21"/>
        <source>Radio IP address, UDP Port Numbers</source>
        <translation>电台设备IP地址，UDP端口号</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="22"/>
        <source>Radio Username, Radio Password</source>
        <translation>电台设备用户名，密码</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="23"/>
        <source>Mic and Speaker on THIS PC</source>
        <translation>当前PC的麦克风和扬声器</translation>
    </message>
    <message>
        <location filename="../firsttimesetup.cpp" line="44"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
</context>
<context>
    <name>aboutbox</name>
    <message>
        <location filename="../aboutbox.ui" line="14"/>
        <source>Form</source>
        <translation>表单</translation>
    </message>
    <message>
        <location filename="../aboutbox.ui" line="48"/>
        <source>wfview version</source>
        <translation>wfview 版本</translation>
    </message>
    <message>
        <location filename="../aboutbox.ui" line="55"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;Detailed text here&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;详细信息&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../aboutbox.ui" line="69"/>
        <source>Build String</source>
        <translation>构建字符串</translation>
    </message>
</context>
<context>
    <name>bandbuttons</name>
    <message>
        <location filename="../bandbuttons.ui" line="14"/>
        <source>Form</source>
        <translation>频段选择面板</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="20"/>
        <source>Band</source>
        <translation>频段</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="49"/>
        <source>2200m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="71"/>
        <source>630m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="93"/>
        <source>160m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="96"/>
        <source>L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="118"/>
        <source>80m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="121"/>
        <source>8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="147"/>
        <source>60m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="150"/>
        <source>S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="172"/>
        <source>40m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="175"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="197"/>
        <source>30m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="200"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="222"/>
        <source>20m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="225"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="251"/>
        <source>17m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="254"/>
        <source>7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="276"/>
        <source>15m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="279"/>
        <source>5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="301"/>
        <source>12m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="304"/>
        <source>T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="326"/>
        <source>10m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="329"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="355"/>
        <source>6m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="358"/>
        <source>6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="380"/>
        <source>4m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="383"/>
        <source>$</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="405"/>
        <source>2m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="408"/>
        <source>V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="430"/>
        <source>70cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="433"/>
        <source>U</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="455"/>
        <source>23cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="484"/>
        <source>13cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="506"/>
        <source>6cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="528"/>
        <source>3cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="554"/>
        <source>WFM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="557"/>
        <source>W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="579"/>
        <source>Air</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="582"/>
        <source>A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="604"/>
        <source>Gen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="607"/>
        <source>G</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="625"/>
        <source>Segment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="631"/>
        <source>&amp;Last Used</source>
        <translation>最近使用(&amp;L)</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="647"/>
        <source>Band Stack Selection:</source>
        <translation>频段堆栈选择：</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="655"/>
        <source>1 - Latest Used</source>
        <translation>1 - 最近使用</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="660"/>
        <source>2 - Older</source>
        <translation>2 - 较早</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="665"/>
        <source>3 - Oldest Used</source>
        <translation>3 - 最早使用</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="673"/>
        <source>Voice</source>
        <translation>语音</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="680"/>
        <source>Data</source>
        <translation>数据</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="687"/>
        <source>&amp;CW</source>
        <translation>CW(&amp;C)</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="694"/>
        <source>Use this button to set the current bandstack register frequency/mode</source>
        <translation>使用此按钮设置当前频段堆栈寄存器的频率/模式</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="697"/>
        <source>Set to current freq/mode</source>
        <translation>设置为当前频率/模式</translation>
    </message>
    <message>
        <location filename="../bandbuttons.ui" line="717"/>
        <source>Work on Sub Band</source>
        <translation>在子频段上工作</translation>
    </message>
</context>
<context>
    <name>calibrationWindow</name>
    <message>
        <location filename="../calibrationwindow.ui" line="32"/>
        <location filename="../calibrationwindow.ui" line="56"/>
        <source>Reference Adjustment</source>
        <translation>参考调整</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="49"/>
        <source>IC-9700</source>
        <translation>IC-9700</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="69"/>
        <source>Course</source>
        <translation>粗调</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="76"/>
        <source>Fine</source>
        <translation>微调</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="163"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Save the calibration data to the indicated slot in the preference file. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;将校准数据保存到首选项文件中指定的槽位。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="166"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="176"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Load the calibration data from the indicated slot in the preference file. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;从首选项文件中指定的槽位加载校准数据。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="179"/>
        <source>Load</source>
        <translation>加载</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="189"/>
        <source>Slot:</source>
        <translation>槽位：</translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="200"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="205"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="210"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="215"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="220"/>
        <source>5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../calibrationwindow.ui" line="243"/>
        <source>Read Current Rig Calibration</source>
        <translation>读取当前设备校准</translation>
    </message>
</context>
<context>
    <name>controllerSetup</name>
    <message>
        <location filename="../controllersetup.ui" line="14"/>
        <source>Controller setup</source>
        <translation>控制器设置</translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="27"/>
        <source>Tab 1</source>
        <translation>标签页 1</translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="32"/>
        <source>Tab 2</source>
        <translation>标签页 2</translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="57"/>
        <source>Backup</source>
        <translation>备份</translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="64"/>
        <source>Restore</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="84"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../controllersetup.ui" line="91"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>cwSender</name>
    <message>
        <location filename="../cwsender.ui" line="14"/>
        <source>MainWindow</source>
        <translation>CW操作面板</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="17"/>
        <source>Send the text in the edit box</source>
        <translation>发送编辑框中的文本</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="24"/>
        <source>Macros</source>
        <translation>宏</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="30"/>
        <location filename="../cwsender.ui" line="40"/>
        <location filename="../cwsender.ui" line="50"/>
        <location filename="../cwsender.ui" line="70"/>
        <location filename="../cwsender.ui" line="80"/>
        <location filename="../cwsender.ui" line="90"/>
        <location filename="../cwsender.ui" line="100"/>
        <location filename="../cwsender.ui" line="110"/>
        <location filename="../cwsender.ui" line="120"/>
        <location filename="../cwsender.ui" line="130"/>
        <source>Macro Access Button</source>
        <translation>宏访问按钮</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="33"/>
        <source>Macro 5</source>
        <translation>宏 5</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="43"/>
        <source>Macro 2</source>
        <translation>宏 2</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="53"/>
        <source>Macro 4</source>
        <translation>宏 4</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="60"/>
        <source>Check this box to enter edit mode, where you can then press the macro buttons to edit the macros.</source>
        <translation>勾选此框进入编辑模式，然后您可以按下宏按钮来编辑宏。</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="63"/>
        <source>Edit Mode</source>
        <translation>编辑模式</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="73"/>
        <source>Macro 7</source>
        <translation>宏 7</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="83"/>
        <source>Macro 3</source>
        <translation>宏 3</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="93"/>
        <source>Macro 6</source>
        <translation>宏 6</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="103"/>
        <source>Macro 10</source>
        <translation>宏 10</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="113"/>
        <source>Macro 9</source>
        <translation>宏 9</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="123"/>
        <source>Macro 8</source>
        <translation>宏 8</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="133"/>
        <source>Macro 1</source>
        <translation>宏 1</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="142"/>
        <source>Seq</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="149"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sequence number, for contests. &lt;/p&gt;&lt;p&gt;Substitute &amp;quot;%1&amp;quot; in your macro text to use it. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;比赛用的序号。&lt;/p&gt;&lt;p&gt;在宏文本中使用 &amp;quot;%1&amp;quot; 来替换。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="170"/>
        <source>CW Transmission Transcript</source>
        <translation>CW 发送记录</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="211"/>
        <source>Stop sending CW</source>
        <translation>停止发送 CW</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="217"/>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="224"/>
        <source>Local Sidetone Level</source>
        <translation>本地侧音电平</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="231"/>
        <source>Local sidetone generator volume</source>
        <translation>本地侧音发生器音量</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="244"/>
        <source>Enable local sidetone generator</source>
        <translation>启用本地侧音发生器</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="247"/>
        <source>Enable</source>
        <translation>启用</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="282"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="285"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="301"/>
        <source>Type here to send text as CW</source>
        <translation>在此输入要以 CW 发送的文本</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="335"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the desired break-in mode:&lt;/p&gt;&lt;p&gt;1. None: You must manually key and unkey the radio.&lt;/p&gt;&lt;p&gt;2. Semi: Transmit is automatic and switches to receive at the end of the text.&lt;/p&gt;&lt;p&gt;3. Full: Same as semi, but with breaks between characters when possible.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;设置所需的插入模式：&lt;/p&gt;&lt;p&gt;1. 关闭：您必须手动按键和释放电台。&lt;/p&gt;&lt;p&gt;2. 半自动：发送自动进行，文本结束时切换到接收。&lt;/p&gt;&lt;p&gt;3. 全自动：与半自动相同，但在可能的情况下在字符之间插入间隔。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="339"/>
        <source>Off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="344"/>
        <source>Semi</source>
        <translation>半自动</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="349"/>
        <source>Full</source>
        <translation>全自动</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="360"/>
        <source>PITCH  (Hz):</source>
        <translation>音调 (Hz)：</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="370"/>
        <source>WPM:</source>
        <translation>字每分：</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="383"/>
        <source>Sets the pitch</source>
        <translation>设置音调</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="408"/>
        <source>Break In</source>
        <translation>插入</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="421"/>
        <source>Sets the dash ratio</source>
        <translation>设置划线比例</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="446"/>
        <source>Dash Ratio</source>
        <translation>划线比例</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="459"/>
        <source>Set the Words Per Minute</source>
        <translation>设置每分钟字数</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="475"/>
        <source>Replace numbers with short letters, for example 9 becomes N</source>
        <translation>用短字母替换数字，例如 9 变成 N</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="478"/>
        <source>Cut Num</source>
        <translation>简化数字</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="485"/>
        <source>Send immediately: Don&apos;t wait for enter, send characters as they are typed</source>
        <translation>立即发送：不等待回车，输入字符时就发送</translation>
    </message>
    <message>
        <location filename="../cwsender.ui" line="488"/>
        <source>Send Immed</source>
        <translation>立即发送</translation>
    </message>
</context>
<context>
    <name>debugWindow</name>
    <message>
        <location filename="../debugwindow.ui" line="14"/>
        <source>Dialog</source>
        <translation>调试面板</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="22"/>
        <source>Current cache items in cachingView()</source>
        <translation>cachingView() 中的当前缓存项</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="32"/>
        <source>Current queue items in cachingView()</source>
        <translation>cachingView() 中的当前队列项</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="67"/>
        <location filename="../debugwindow.ui" line="107"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="72"/>
        <location filename="../debugwindow.ui" line="112"/>
        <source>Function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="77"/>
        <location filename="../debugwindow.ui" line="132"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="82"/>
        <location filename="../debugwindow.ui" line="127"/>
        <source>RX</source>
        <translation>接收</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="87"/>
        <source>Request</source>
        <translation>请求</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="92"/>
        <source>Reply</source>
        <translation>回复</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="117"/>
        <source>Priority</source>
        <translation>优先级</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="122"/>
        <source>Get/Set</source>
        <translation>获取/设置</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="137"/>
        <source>Recurring</source>
        <translation>循环</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="149"/>
        <location filename="../debugwindow.ui" line="186"/>
        <source>Pause refresh</source>
        <translation>暂停刷新</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="169"/>
        <location filename="../debugwindow.ui" line="206"/>
        <source>Refresh Interval (ms)</source>
        <translation>刷新间隔（毫秒）</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="176"/>
        <location filename="../debugwindow.ui" line="219"/>
        <source>99999</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="179"/>
        <source>500</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="222"/>
        <source>1000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="245"/>
        <source>Scroll test:</source>
        <translation>滚动测试：</translation>
    </message>
    <message>
        <location filename="../debugwindow.ui" line="274"/>
        <source>TextLabel</source>
        <translation>文本标签</translation>
    </message>
</context>
<context>
    <name>frequencyinputwidget</name>
    <message>
        <location filename="../frequencyinputwidget.ui" line="14"/>
        <source>Form</source>
        <translation>直接输入频率面板</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="22"/>
        <source>Frequency:</source>
        <translation>频率：</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="33"/>
        <source>Go</source>
        <translation>转到</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="36"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="48"/>
        <source>Entry</source>
        <translation>输入</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="66"/>
        <location filename="../frequencyinputwidget.ui" line="69"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="88"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To recall a preset memory:&lt;/p&gt;&lt;p&gt;1. Type in the preset number (0 through 99)&lt;/p&gt;&lt;p&gt;2. Press RCL (or use hotkey &amp;quot;R&amp;quot;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;调用预设记忆：&lt;/p&gt;&lt;p&gt;1. 输入预设号码（0 到 99）&lt;/p&gt;&lt;p&gt;2. 按 RCL（或使用热键 &amp;quot;R&amp;quot;）&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="91"/>
        <source>&amp;RCL</source>
        <translation>调用(&amp;R)</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="94"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="113"/>
        <location filename="../frequencyinputwidget.ui" line="116"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="135"/>
        <location filename="../frequencyinputwidget.ui" line="138"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="160"/>
        <source>&amp;CE</source>
        <translation>清除(&amp;C)</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="163"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="182"/>
        <location filename="../frequencyinputwidget.ui" line="185"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="204"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To store a preset:&lt;/p&gt;&lt;p&gt;1. Set the desired frequency and mode per normal methods&lt;/p&gt;&lt;p&gt;2. Type the index to to store to (0 through 99)&lt;/p&gt;&lt;p&gt;3. Press STO (or use hotkey &amp;quot;S&amp;quot;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;存储预设：&lt;/p&gt;&lt;p&gt;1. 使用常规方法设置所需的频率和模式&lt;/p&gt;&lt;p&gt;2. 输入要存储的索引（0 到 99）&lt;/p&gt;&lt;p&gt;3. 按 STO（或使用热键 &amp;quot;S&amp;quot;）&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="207"/>
        <source>&amp;STO</source>
        <translation>存储(&amp;S)</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="210"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="229"/>
        <location filename="../frequencyinputwidget.ui" line="232"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="251"/>
        <source>Back</source>
        <translation>退格</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="254"/>
        <source>Backspace</source>
        <translation>退格</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="273"/>
        <location filename="../frequencyinputwidget.ui" line="276"/>
        <source>Enter</source>
        <translation>回车</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="295"/>
        <location filename="../frequencyinputwidget.ui" line="298"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="317"/>
        <location filename="../frequencyinputwidget.ui" line="320"/>
        <source>.</source>
        <translation>.</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="339"/>
        <location filename="../frequencyinputwidget.ui" line="342"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="361"/>
        <location filename="../frequencyinputwidget.ui" line="364"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="383"/>
        <location filename="../frequencyinputwidget.ui" line="386"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../frequencyinputwidget.ui" line="405"/>
        <location filename="../frequencyinputwidget.ui" line="408"/>
        <source>8</source>
        <translation>8</translation>
    </message>
</context>
<context>
    <name>loggingWindow</name>
    <message>
        <location filename="../loggingwindow.ui" line="14"/>
        <source>Form</source>
        <translation>运行日志面板</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="56"/>
        <source>Annotation:</source>
        <translation>注释：</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="69"/>
        <source>You may enter your own log notes here.</source>
        <translation>您可以在此输入您的日志记录。</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="88"/>
        <source>Adds user-text to the log.</source>
        <translation>添加用户文本到日志。</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="91"/>
        <source>Annotate</source>
        <translation>添加注释</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="108"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable or disable debug logging. Use the &amp;quot;-d&amp;quot; or &amp;quot;--debug&amp;quot; flag to open wfview with debug logging enabled on startup. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;启用或禁用调试日志记录。使用 &amp;quot;-d&amp;quot; 或 &amp;quot;--debug&amp;quot; 标志在启动时启用调试日志记录打开 wfview。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="111"/>
        <source>Debug</source>
        <translation>调试</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="118"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This enables the logging of nearly all CI-V traffic. &lt;span style=&quot; font-weight:600;&quot;&gt;Use with caution&lt;/span&gt;. It is a lot of data. Meter levels and scope data are not shown. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;这将启用几乎所有 CI-V 通信的日志记录。&lt;span style=&quot; font-weight:600;&quot;&gt;请谨慎使用&lt;/span&gt;。数据量很大。不显示仪表级别和频谱数据。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="121"/>
        <source>CommDebug</source>
        <translation>通信调试</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="128"/>
        <source>RigCtl Debug</source>
        <translation>设备控制调试</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="135"/>
        <source>Scroll to bottom</source>
        <translation>滚动到底部</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="138"/>
        <source>Scroll Down</source>
        <translation>向下滚动</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="145"/>
        <source>Clears the display. Does not clear the log file.</source>
        <translation>清除显示。不会清除日志文件。</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="148"/>
        <source>Clear</source>
        <translation>清除</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="155"/>
        <source>Makes a best-effort to ask the host system to open the log file directory.</source>
        <translation>尽最大努力请求主机系统打开日志文件目录。</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="158"/>
        <source>Open Log Directory</source>
        <translation>打开日志目录</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="165"/>
        <source>Makes a best-effort to ask the host system to open the logfile.</source>
        <translation>尽最大努力请求主机系统打开日志文件。</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="168"/>
        <source>Open Log</source>
        <translation>打开日志</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="175"/>
        <source>Copy the path of the log file to your clipboard.</source>
        <translation>将日志文件的路径复制到剪贴板。</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="178"/>
        <source>Copy Path</source>
        <translation>复制日志路径</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="185"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sends text to termbin.com. Some personal information (such as your username) is in the log file, so do not click this button unless you are ok sharing your log file. This is a quick way to receive a URL, pointing to your log file text, that you can send to other people. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;将文本发送到 termbin.com。日志文件中包含一些个人信息（如您的用户名），因此除非您同意共享您的日志文件，否则请不要点击此按钮。这是一种快速获取指向您的日志文件文本的 URL 的方法，您可以将其发送给其他人。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../loggingwindow.ui" line="188"/>
        <source>Send to termbin.com</source>
        <translation>发送到 termbin.com</translation>
    </message>
</context>
<context>
    <name>memories</name>
    <message>
        <location filename="../memories.ui" line="14"/>
        <source>Memory Management</source>
        <translation>记忆管理</translation>
    </message>
    <message>
        <location filename="../memories.ui" line="43"/>
        <source>Disable Editing</source>
        <translation>禁用编辑</translation>
    </message>
    <message>
        <location filename="../memories.ui" line="66"/>
        <source>Start Scan</source>
        <translation>开始扫描</translation>
    </message>
    <message>
        <location filename="../memories.ui" line="86"/>
        <source>.csv Import</source>
        <translation>导入 CSV</translation>
    </message>
    <message>
        <location filename="../memories.ui" line="93"/>
        <source>All fields</source>
        <translation>所有字段</translation>
    </message>
    <message>
        <location filename="../memories.ui" line="103"/>
        <source>.csv Export</source>
        <translation>导出 CSV</translation>
    </message>
    <message>
        <location filename="../memories.ui" line="123"/>
        <source>Select Memory Mode</source>
        <translation>选择记忆模式</translation>
    </message>
</context>
<context>
    <name>meter</name>
    <message>
        <location filename="../meter.cpp" line="314"/>
        <source>Double-click to set meter</source>
        <translation>双击设置仪表</translation>
    </message>
</context>
<context>
    <name>pttyHandler</name>
    <message>
        <location filename="../pttyhandler.cpp" line="205"/>
        <source>Read failed: %1</source>
        <translation>读取失败：%1</translation>
    </message>
</context>
<context>
    <name>receiverWidget</name>
    <message>
        <location filename="../receiverwidget.cpp" line="31"/>
        <location filename="../receiverwidget.cpp" line="46"/>
        <source>VFO A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="44"/>
        <source>VFO B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="50"/>
        <source>A&lt;&gt;B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="60"/>
        <source>A=B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="70"/>
        <source>SPLIT</source>
        <translation>异频</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="133"/>
        <source>Detach</source>
        <translation>拆分面板</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="135"/>
        <source>Detach/re-attach scope from main window</source>
        <translation>从主窗口分离/重新附加示波器</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="139"/>
        <source>Spectrum Mode</source>
        <translation>频谱模式</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="142"/>
        <source>Spectrum Span</source>
        <translation>频谱跨度</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="146"/>
        <source>Spectrum Edge</source>
        <translation>频谱边缘</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="147"/>
        <source>Custom Edge</source>
        <translation>自定义边缘</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="148"/>
        <source>Define a custom (fixed) scope edge</source>
        <translation>定义自定义（固定）示波器边缘</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="149"/>
        <source>To Fixed</source>
        <translation>转为固定</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="150"/>
        <source>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Press button to convert center mode spectrum to fixed mode, preserving the range. This allows you to tune without the spectrum moving, in the same currently-visible range that you see now. &amp;lt;/p&amp;gt;&amp;lt;p&amp;gt;&amp;lt;br/&amp;gt;&amp;lt;/p&amp;gt;&amp;lt;p&amp;gt;The currently-selected edge slot will be overridden.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;按下按钮将中心模式频谱转换为固定模式，保持范围不变。这样您可以在当前可见范围内调谐而不移动频谱。&amp;lt;/p&amp;gt;&amp;lt;p&amp;gt;&amp;lt;br/&amp;gt;&amp;lt;/p&amp;gt;&amp;lt;p&amp;gt;当前选择的边缘槽将被覆盖。&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="162"/>
        <location filename="../receiverwidget.cpp" line="164"/>
        <source>Configure Scope</source>
        <translation>配置示波器</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="163"/>
        <source>Change various settings of the current Scope</source>
        <translation>更改当前示波器的各种设置</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="214"/>
        <source>Center Mode</source>
        <translation>中心模式</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="215"/>
        <source>Fixed Mode</source>
        <translation>固定模式</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="216"/>
        <source>Scroll-C</source>
        <translation>滚动-中心</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="217"/>
        <source>Scroll-F</source>
        <translation>滚动-固定</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="220"/>
        <source>Fixed Edge 1</source>
        <translation>固定边缘 1</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="220"/>
        <source>Fixed Edge 2</source>
        <translation>固定边缘 2</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="220"/>
        <source>Fixed Edge 3</source>
        <translation>固定边缘 3</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="220"/>
        <source>Fixed Edge 4</source>
        <translation>固定边缘 4</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="249"/>
        <source>SCOPE OUT OF RANGE</source>
        <translation>示波器超出范围</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="260"/>
        <source> OVF </source>
        <translation>溢出</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="325"/>
        <source>Scope display reference</source>
        <translation>示波器显示参考</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="326"/>
        <source>Selects the display reference for the Scope display</source>
        <translation>选择示波器显示的参考值</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="327"/>
        <source>Select display reference of scope</source>
        <translation>选择示波器的显示参考</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="328"/>
        <source>Ref</source>
        <translation>参考</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="332"/>
        <source>Length</source>
        <translation>长度</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="337"/>
        <source>Scope display ceiling</source>
        <translation>示波器显示上限</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="338"/>
        <source>Selects the display ceiling for the Scope display</source>
        <translation>选择示波器显示的上限值</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="339"/>
        <source>Select display ceiling of scope</source>
        <translation>选择示波器的显示上限</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="340"/>
        <source>Ceiling</source>
        <translation>上限</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="345"/>
        <source>Scope display floor</source>
        <translation>示波器显示下限</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="346"/>
        <source>Selects the display floor for the Scope display</source>
        <translation>选择示波器显示的下限值</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="347"/>
        <source>Select display floor of scope</source>
        <translation>选择示波器的显示下限</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="348"/>
        <source>Floor</source>
        <translation>下限</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="351"/>
        <source>Speed Fast</source>
        <translation>快速</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="352"/>
        <source>Speed Mid</source>
        <translation>中速</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="353"/>
        <source>Speed Slow</source>
        <translation>慢速</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="355"/>
        <source>Waterfall display speed</source>
        <translation>瀑布图显示速度</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="356"/>
        <source>Selects the speed for the waterfall display</source>
        <translation>选择瀑布图的显示速度</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="357"/>
        <source>Waterfall Speed</source>
        <translation>瀑布图速度</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="358"/>
        <source>Speed</source>
        <translation>速度</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="361"/>
        <source>Waterfall display color theme</source>
        <translation>瀑布图显示颜色主题</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="362"/>
        <source>Selects the color theme for the waterfall display</source>
        <translation>选择瀑布图的显示颜色主题</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="363"/>
        <source>Waterfall color theme</source>
        <translation>瀑布图颜色主题</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="377"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="381"/>
        <source>PBT Inner</source>
        <translation>PBT 内部</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="385"/>
        <source>PBT Outer</source>
        <translation>PBT 外部</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="399"/>
        <source>IF Shift</source>
        <translation>IF 移位</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="403"/>
        <source>Fill Width</source>
        <translation>填充宽度</translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="1244"/>
        <source>Scope Edges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="1245"/>
        <source>Please enter desired scope edges, in MHz,
with a comma between the low and high range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../receiverwidget.cpp" line="1273"/>
        <source>Error, could not interpret your input.                          &lt;br/&gt;Please make sure to place a comma between the frequencies.                          &lt;br/&gt;For example: &apos;7.200, 7.300&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>repeaterSetup</name>
    <message>
        <location filename="../repeatersetup.ui" line="26"/>
        <source>Repeater Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="47"/>
        <source>Repeater Duplex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="53"/>
        <source>Simplex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="63"/>
        <source>Dup+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="73"/>
        <source>Dup-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="83"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="95"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the repeater offset for radios using Repeater modes. Only available on radios that have repeater modes. Radios using Split should instead use the provided Split Mode section with a custom Offset. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="98"/>
        <source>Set Offset (MHz):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="107"/>
        <source>Rpt Offset (MHz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="123"/>
        <source>Split Mode</source>
        <translation>异频模式</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="131"/>
        <source>Turn on Split</source>
        <translation>开启异频功能</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="134"/>
        <source>Split On</source>
        <translation>开启</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="144"/>
        <source>Turn off Split</source>
        <translation>关闭异频功能</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="147"/>
        <source>Split Off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="157"/>
        <source>QuickSplit</source>
        <translation>快速</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="168"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Click here to automatically set the sub VFO (transmit VFO) tone. Only available on some radios. Other radios may take care of this for you.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="171"/>
        <source>Set Rpt Tone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="181"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Click here to continually set the transmit VFO to match the receive VFO with the offset accounted for.  &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="184"/>
        <source>AutoTrack</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="195"/>
        <source>Offset (KHz):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="214"/>
        <source>Enter the desired split offset in KHz.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="227"/>
        <location filename="../repeatersetup.ui" line="243"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the transmit frequency to the receive frequency PLUS the offset. Sets the radio sub VFO and also populates the wfview text box (as a convenience). &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="230"/>
        <source>Split+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="246"/>
        <source>Split-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="257"/>
        <source>Tx Freq (MHz):</source>
        <translation>Tx 频率(MHz)</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="286"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the transmit frequency manually. Not needed if the Split+ or Split- button was used. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="289"/>
        <source>Set</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="307"/>
        <source>VFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="313"/>
        <source>Swap VFO A with VFO B. Some radios do not support this.</source>
        <translation>交换 VFO A 和VFO B。部分设备不支持该功能。</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="316"/>
        <source>Swap AB</source>
        <translation>交换AB</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="323"/>
        <source>Select the Sub VFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="326"/>
        <source>Sel Sub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="333"/>
        <source>Select the Main VFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="336"/>
        <source>Sel Main</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="343"/>
        <source>Select VFO B</source>
        <translation>使用 VFO B</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="346"/>
        <source>Sel B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="353"/>
        <source>Select VFO A</source>
        <translation>使用 VFO A</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="356"/>
        <source>Sel A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="363"/>
        <source>Set VFO B to VFO A</source>
        <translation>把VFO B 设置到 VFO A</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="366"/>
        <source>A=B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="373"/>
        <source>Set the SUB VFO to match the Main VFO</source>
        <translation>把SUB VFO设置到Main VFO</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="376"/>
        <source>M=&gt;S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="383"/>
        <source>Swap the Main VFO and Sub VFO</source>
        <translation>交换Main VFO和Sub VFO</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="386"/>
        <source>Swap MS</source>
        <translation>交换 MS</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="402"/>
        <source>Repeater Tone Type</source>
        <translation>Repeater 亚音设置</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="408"/>
        <source>Only available in FM</source>
        <translation>仅适用于FM</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="415"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="425"/>
        <source>Transmit Tone only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="435"/>
        <source>Tone Squelch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="445"/>
        <location filename="../repeatersetup.ui" line="496"/>
        <source>DTCS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="455"/>
        <source>Set the Tone Mode for the Sub VFO. Not available on all radios.</source>
        <translation>设置Sub VFO的亚音模式。仅适用于部分电台。</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="458"/>
        <location filename="../repeatersetup.ui" line="529"/>
        <source>Set Sub VFO</source>
        <translation>设置Sub VFO</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="474"/>
        <source>Tone Selection</source>
        <translation>亚音选择</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="482"/>
        <source>Tone</source>
        <translation>亚音</translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="510"/>
        <source>Invert Tx</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="517"/>
        <source>Invert Rx</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../repeatersetup.ui" line="526"/>
        <source>Set the Sub VFO to the selected Tone. Not available on all radios and not available for DTCS.</source>
        <translation>将Sub VFO设置为选定的亚音模式。并非所有无线电设备都具备此功能，且该功能不适用于DTCS。</translation>
    </message>
</context>
<context>
    <name>rigCreator</name>
    <message>
        <location filename="../rigcreator.ui" line="20"/>
        <source>Rig Creator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="32"/>
        <source>Memories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="40"/>
        <source>Grp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="47"/>
        <location filename="../rigcreator.ui" line="1168"/>
        <source>Mem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="54"/>
        <location filename="../rigcreator.ui" line="61"/>
        <location filename="../rigcreator.ui" line="68"/>
        <location filename="../rigcreator.ui" line="89"/>
        <source>999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="75"/>
        <source>Sat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="82"/>
        <location filename="../rigcreator.ui" line="1153"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="101"/>
        <source>Commands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="156"/>
        <location filename="../rigcreator.ui" line="275"/>
        <source>Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="161"/>
        <source>String</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="166"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="171"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="176"/>
        <source>29</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="181"/>
        <source>G/S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="198"/>
        <source>Preamps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="241"/>
        <location filename="../rigcreator.ui" line="389"/>
        <location filename="../rigcreator.ui" line="453"/>
        <location filename="../rigcreator.ui" line="517"/>
        <location filename="../rigcreator.ui" line="573"/>
        <location filename="../rigcreator.ui" line="946"/>
        <location filename="../rigcreator.ui" line="1010"/>
        <location filename="../rigcreator.ui" line="1143"/>
        <source>Num</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="246"/>
        <location filename="../rigcreator.ui" line="399"/>
        <location filename="../rigcreator.ui" line="458"/>
        <location filename="../rigcreator.ui" line="522"/>
        <location filename="../rigcreator.ui" line="583"/>
        <location filename="../rigcreator.ui" line="951"/>
        <location filename="../rigcreator.ui" line="1030"/>
        <location filename="../rigcreator.ui" line="1173"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="257"/>
        <source>Periodic Commands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="270"/>
        <source>Priority</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="280"/>
        <source>VFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="295"/>
        <source>Main Memory Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="308"/>
        <location filename="../rigcreator.ui" line="332"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Format %&amp;lt;start&amp;gt;.&amp;lt;len&amp;gt;&amp;lt;specifier&amp;gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;specifier from list below:&lt;/p&gt;&lt;p&gt;a = Group&lt;/p&gt;&lt;p&gt;b = Num&lt;/p&gt;&lt;p&gt;c = Scan&lt;/p&gt;&lt;p&gt;d = Scan/Split&lt;/p&gt;&lt;p&gt;D = Duplex Setting (use j for most rigs)&lt;/p&gt;&lt;p&gt;e = VFO A E = VFO B&lt;/p&gt;&lt;p&gt;f = Frequency A F = Frequency B&lt;/p&gt;&lt;p&gt;g = Mode A G = Mode B&lt;/p&gt;&lt;p&gt;h = Filter H = Filter B&lt;/p&gt;&lt;p&gt;i = Data I = Data B&lt;/p&gt;&lt;p&gt;j = Duplex/Tonemode A J = Duplex B/Tonemode B&lt;/p&gt;&lt;p&gt;k = Data/Tonemode A K = Data B/Tonemode B&lt;/p&gt;&lt;p&gt;l = Tonemode A L = Tonemode B&lt;/p&gt;&lt;p&gt;m = DSQL A M = DSQL B&lt;/p&gt;&lt;p&gt;n = Tone type A N = Tone type B&lt;/p&gt;&lt;p&gt;o = TSQL A O = TSQL B&lt;/p&gt;&lt;p&gt;p = DTCS Polarity A P = DTCS Polarity B&lt;/p&gt;&lt;p&gt;q = DTCS A Q = DTCS B&lt;/p&gt;&lt;p&gt;r = DV Squelch A R = DV Squelch B&lt;/p&gt;&lt;p&gt;s = Duplex Offset A S = Duplex Offset B&lt;/p&gt;&lt;p&gt;t = DV UR A T = DV UR B&lt;/p&gt;&lt;p&gt;u = DV R1 A U = DV R1 B&lt;/p&gt;&lt;p&gt;v = DV R2 A V = DV R2 B&lt;/p&gt;&lt;p&gt;w = Tuning Step (+custom)&lt;/p&gt;&lt;p&gt;x = Preamp + Attenuator&lt;/p&gt;&lt;p&gt;y = Antenna&lt;/p&gt;&lt;p&gt;+ = IP Plus&lt;/p&gt;&lt;p&gt;z = Memory Name&lt;/p&gt;&lt;p&gt;Z = Mode specific columns&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="319"/>
        <source>Satellite Memory Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="343"/>
        <source>Inputs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="394"/>
        <location filename="../rigcreator.ui" line="1015"/>
        <source>Reg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="410"/>
        <source>Spans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="463"/>
        <source>Freq</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="474"/>
        <source>Antennas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="533"/>
        <source>Flters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="578"/>
        <location filename="../rigcreator.ui" line="967"/>
        <source>Modes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="600"/>
        <source>Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="606"/>
        <source>Has Spectrum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="613"/>
        <source>Has LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="620"/>
        <source>Has Ethernet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="627"/>
        <source>Has WiFi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="634"/>
        <source>Has Transmit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="641"/>
        <source>HasFDComms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="648"/>
        <source>Has Command 29</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="662"/>
        <source>Load File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="669"/>
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="689"/>
        <source>Default Rigs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="696"/>
        <source>Manufacturer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="704"/>
        <source>Icom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="709"/>
        <source>Yaesu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="714"/>
        <source>Kenwood</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="722"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="732"/>
        <source>C-IV Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="742"/>
        <source>RigCtlD Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="749"/>
        <source>9999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="766"/>
        <source>Spectrum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="774"/>
        <source>Seq Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="781"/>
        <source>Len Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="788"/>
        <source>Amp Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="807"/>
        <location filename="../rigcreator.ui" line="826"/>
        <source>000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="851"/>
        <source>00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="858"/>
        <source>Num RX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="871"/>
        <location filename="../rigcreator.ui" line="891"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="878"/>
        <source>VFO per RX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="903"/>
        <source>Tuning Steps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="956"/>
        <source>Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1020"/>
        <source>Min BW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1025"/>
        <source>Max BW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1041"/>
        <source>Atten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1081"/>
        <source>dB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1092"/>
        <source>Bands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1138"/>
        <source>Region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1148"/>
        <source>BSR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1158"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1163"/>
        <source>Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1178"/>
        <source>Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1183"/>
        <source>Pwr (W)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1188"/>
        <source>Ant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.ui" line="1193"/>
        <source>Colour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.cpp" line="123"/>
        <location filename="../rigcreator.cpp" line="126"/>
        <location filename="../rigcreator.cpp" line="149"/>
        <location filename="../rigcreator.cpp" line="151"/>
        <location filename="../rigcreator.cpp" line="583"/>
        <location filename="../rigcreator.cpp" line="585"/>
        <source>Select Rig Filename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.cpp" line="177"/>
        <source>Not a rig definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.cpp" line="178"/>
        <source>File %0 does not appear to be a valid Rig definition file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.cpp" line="880"/>
        <source>rig creator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rigcreator.cpp" line="880"/>
        <source>Changes will be lost!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>satelliteSetup</name>
    <message>
        <location filename="../satellitesetup.ui" line="32"/>
        <source>Satellite Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="101"/>
        <source>Satellite Setup:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="141"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="148"/>
        <source>Linear Inverting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="155"/>
        <source>Linear Non-Inverting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="162"/>
        <source>FM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="189"/>
        <source>Uplink:</source>
        <translation>上行</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="212"/>
        <source>Downlink:</source>
        <translation>下行</translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="258"/>
        <source>Uplink from:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="281"/>
        <location filename="../satellitesetup.ui" line="356"/>
        <source>To:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="333"/>
        <source>Downlink from:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="408"/>
        <source>Telemetry: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="451"/>
        <source>Additional Spectrum Margin (KHz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="468"/>
        <source>(added to both sides)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="495"/>
        <source>Set VFOs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="502"/>
        <source>Set Spectrum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../satellitesetup.ui" line="509"/>
        <source>Add Markers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>selectRadio</name>
    <message>
        <location filename="../selectradio.ui" line="14"/>
        <source>Select Radio From List</source>
        <translation>选择电台</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="35"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="58"/>
        <source>Rig Name</source>
        <translation>电台名称</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="63"/>
        <source>CI-V</source>
        <translation>CI-V 地址</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="68"/>
        <source>Baud Rate</source>
        <translation>波特率</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="73"/>
        <source>Current User</source>
        <translation>当前用户</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="78"/>
        <source>User IP Address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="129"/>
        <source>AF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../selectradio.ui" line="162"/>
        <source>MOD</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>settingswidget</name>
    <message>
        <location filename="../settingswidget.ui" line="14"/>
        <source>Settings</source>
        <translation>配置面板</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="59"/>
        <source>Radio Connection</source>
        <translation>如何连接电台</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="65"/>
        <source>Serial (USB)</source>
        <translation>串口(USB)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="72"/>
        <source>Network</source>
        <translation>网络</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="94"/>
        <source>CI-V and Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="100"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you are using an older (year 2010) radio, you may need to enable this option to manually specify the CI-V address. This option is also useful for radios that do not have CI-V Transceive enabled and thus will not answer our broadcast query for connected rigs on the CI-V bus.&lt;/p&gt;&lt;p&gt;If you have a modern radio with CI-V Transceive enabled, you should not need to check this box. &lt;/p&gt;&lt;p&gt;You will need to Save Settings and re-launch wfview for this to take effect. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;如果您使用的是较旧的（2010年之前）无线电设备，可能需要启用此选项以手动指定 CI-V 地址。此选项同样适用于未启用 CI-V Transceive 功能的无线电设备，因为这些设备不会响应我们对 CI-V 总线中已连接设备的广播查询。&lt;/p&gt;&lt;p&gt;如果您的无线电设备是现代设备且已启用 CI-V Transceive 功能，则无需勾选此选项。&lt;/p&gt;&lt;p&gt;您需要保存设置并重新启动 wfview 以使更改生效。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="103"/>
        <source>Manual Radio CI-V Address:</source>
        <translation>手动指定CI-V 地址</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="110"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only check for older radios!&lt;/p&gt;&lt;p&gt;This checkbox forces wfview to trust that the CI-V address is also the model number of the radio. This is only useful for older radios that do not reply to our Rig ID requests (0x19 0x00). Do not check this box unless you have an older radio. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;仅针对较旧的无线电设备勾选此项！&lt;/p&gt;&lt;p&gt;勾选此复选框将强制 wfview 信任 CI-V 地址也是无线电的型号。此选项仅适用于未响应我们Rig ID 请求（0x19 0x00）的旧款无线电设备。除非您使用的是较旧的无线电设备，否则请勿勾选此框。 &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="113"/>
        <source>Use CI-V address as Model ID too</source>
        <translation>CI-V地址当作Model ID</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter the address in as hexadecimal, without any prefix, just as the radio presents the address in the menu. &lt;/p&gt;&lt;p&gt;Here are some common examples:&lt;/p&gt;
&lt;p&gt;IC-706: 58 
&lt;br/&gt;IC-756: 50
&lt;br/&gt;IC-756 Pro: 5C
&lt;br/&gt;IC-756 Pro II: 64
&lt;br/&gt;IC-756 Pro III: 6E
&lt;br/&gt;IC-7000: 70
&lt;br/&gt;IC-7100: 88
&lt;br/&gt;IC-7200: 76 
&lt;br/&gt;IC-7300: 94
&lt;/p&gt;&lt;p&gt;This setting is typically needed for older radios and for radios that do not have CI-V Transceive enabled. &lt;/p&gt;
&lt;p&gt;After changing, press Save Settings and re-launch wfview.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>以十六进制输入地址，无需任何前缀，直接按照无线电菜单中显示的地址输入即可。</translatorcomment>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;输入十六进制格式的地址，直接按照电台菜单中显示的地址输入即可，不需要任何前缀。&lt;/p&gt;&lt;p&gt;以下是一些常见示例：&lt;/p&gt;
&lt;p&gt;IC-706: 58 
&lt;br/&gt;IC-756: 50
&lt;br/&gt;IC-756 Pro: 5C
&lt;br/&gt;IC-756 Pro II: 64
&lt;br/&gt;IC-756 Pro III: 6E
&lt;br/&gt;IC-7000: 70
&lt;br/&gt;IC-7100: 88
&lt;br/&gt;IC-7200: 76 
&lt;br/&gt;IC-7300: 94
&lt;/p&gt;&lt;p&gt;此设置通常适用于较旧的无线电设备以及未启用 CI-V Transceive 功能的无线电设备。 &lt;/p&gt;
&lt;p&gt;更改后，请点击“保存设置”并重新启动 wfview。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="149"/>
        <source>auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="165"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="176"/>
        <source>Audio controls on this page are ONLY for network radios
Please use the &quot;Radio Server&quot; page to select server audio.
ONLY use Manual CI-V when Transceive mode is not supported</source>
        <translation>此页面上的音频控制仅适用于网络无线电。
请使用“无线电服务器”页面选择服务器音频。
仅在电台不支持时再使用手动 CI-V。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="203"/>
        <source>Serial Connected Radios</source>
        <translation>串口连接设置</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="221"/>
        <source>Serial Device:</source>
        <translation>串口选择</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="244"/>
        <source>Baud Rate</source>
        <translation>波特率</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="261"/>
        <source>PTT Type</source>
        <translation>PPT 类型</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="269"/>
        <source>CI-V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="274"/>
        <source>RTS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="279"/>
        <source>DTR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="316"/>
        <source>Network Connected Radios</source>
        <translation>网络连接设置</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="348"/>
        <source>Hostname</source>
        <translation>主机地址</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="371"/>
        <location filename="../settingswidget.ui" line="2602"/>
        <source>Control Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="384"/>
        <location filename="../settingswidget.ui" line="2630"/>
        <source>50001</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="417"/>
        <location filename="../settingswidget.ui" line="2895"/>
        <location filename="../settingswidget.ui" line="3371"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="440"/>
        <location filename="../settingswidget.ui" line="2900"/>
        <location filename="../settingswidget.ui" line="3381"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="483"/>
        <source>RX Latency (ms)</source>
        <translation>接收延迟 (ms)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="509"/>
        <location filename="../settingswidget.ui" line="536"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="516"/>
        <source>TX Latency (ms)</source>
        <translation>发送延迟 (ms)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="543"/>
        <source>RX Codec</source>
        <translation>RX解码器</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="550"/>
        <source>Receive Audio Codec Selector</source>
        <translation>选择接收音频解码器</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="557"/>
        <source>TX Codec</source>
        <translation>TX编码器</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="564"/>
        <source>Transmit Audio Codec Selector</source>
        <translation>选择发射音频编码器</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="588"/>
        <source>Sample Rate</source>
        <translation>采样频率</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="595"/>
        <source>Audio Sample Rate Selector</source>
        <translation>选择音频采样频率</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="599"/>
        <source>48000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="604"/>
        <source>24000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="609"/>
        <source>16000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="614"/>
        <source>8000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="622"/>
        <source>Duplex</source>
        <translation>全双工</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="630"/>
        <source>Full Duplex</source>
        <translation>全双工</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="635"/>
        <source>Half Duplex</source>
        <translation>半双工</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="643"/>
        <location filename="../settingswidget.ui" line="2804"/>
        <source>Audio System</source>
        <translation>音频系统</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="651"/>
        <location filename="../settingswidget.ui" line="2812"/>
        <source>Qt Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="656"/>
        <location filename="../settingswidget.ui" line="2817"/>
        <source>PortAudio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="661"/>
        <location filename="../settingswidget.ui" line="2822"/>
        <source>RT Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="666"/>
        <source>TCI Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="691"/>
        <source>Audio Output </source>
        <translation>音频输出</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="704"/>
        <source>Audio Output Selector</source>
        <translation>选择输出音频</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="711"/>
        <source>Audio Input</source>
        <translation>输入音频</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="724"/>
        <source>Audio Input Selector</source>
        <translation>选择输入音频</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="751"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;You &lt;span style=&quot; font-weight:700;&quot;&gt;MUST&lt;/span&gt; disconnect from the radio before making any changes in the above form.&lt;br/&gt;&lt;br/&gt;Please use the &lt;span style=&quot; font-style:italic;&quot;&gt;Connect/Disconnect &lt;/span&gt;button below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;您&lt;span style=&quot; font-weight:700;&quot;&gt;必须&lt;/span&gt;在更改上述表单中的任何设置之前断开与无线电的连接。&lt;br/&gt;&lt;br/&gt;请使用下面的&lt;span style=&quot; font-style:italic;&quot;&gt;连接/断开 &lt;/span&gt;按钮。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="783"/>
        <source>When tuning, set lower digits to zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="793"/>
        <location filename="../settingswidget.ui" line="799"/>
        <source>When using SSB, automatically switch to the standard sideband for a given band.</source>
        <translation>使用SSB时，自动切换到给定频段的标准边带</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="796"/>
        <source>Auto SSB Switching</source>
        <translation>自动SSB开关</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="802"/>
        <source>Auto SSB</source>
        <translation>自动SSB</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="809"/>
        <source>Enable PTT Controls</source>
        <translation>激活 PPT 控制</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="816"/>
        <source>Rig creator allows changing of all rig features and adding new rig profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="819"/>
        <source>Enable Rig Creator Feature (use with care)</source>
        <translation>激活Rig Creator功能(谨慎使用)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="839"/>
        <location filename="../settingswidget.ui" line="855"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ITU Region. Used to display band limits. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Region 1&lt;/span&gt; comprises Europe, Africa, the Commonwealth of Independent States, Mongolia, and the Middle East west of the Persian Gulf, including Iraq.&lt;/p&gt;&lt;p&gt;The western boundary is defined by Line B.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Region 2&lt;/span&gt; covers the Americas including Greenland, and some of the eastern Pacific Islands.&lt;/p&gt;&lt;p&gt;The eastern boundary is defined by Line B.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Region 3&lt;/span&gt; contains most of non-FSU Asia east of and including Iran, and most of Oceania.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Line B&lt;/span&gt; is a line running from the North Pole along meridian 10° West of Greenwich to its intersection with parallel 72° North; thence by great circle arc to the intersection of meridian 50° West and parallel 40° North; thence by great circle arc to the intersection of meridian 20° West and parallel 10° South; thence along meridian 20° West to the South Pole.&lt;/p&gt;&lt;p&gt;(Text is from the English wikipedia)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ITU 区域。用于显示频段限制。&lt;/p&gt;&lt;p&gt;粗略的描述：&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;第1区&lt;/span&gt;欧洲、非洲、前苏联地区、蒙古国、波斯湾以西的中东地区。&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;第2区&lt;/span&gt;整个美洲大陆（包括格陵兰）以及东太平洋部分岛屿。&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;第3区&lt;/span&gt; 包含大部分非独联体的亚洲，位于伊朗及以东的地区，以及大部分大洋洲。&lt;/p&gt;&lt;p&gt;中国属于第3区&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="842"/>
        <source>Region:</source>
        <translation>区域</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="861"/>
        <location filename="../settingswidget.ui" line="1282"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="872"/>
        <source>Enables interpolation between pixels. Note that this will increase CPU usage.</source>
        <translation>启用像素间插值。请注意，这将增加 CPU 使用率。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="875"/>
        <source>Interpolate Waterfall</source>
        <translation>瀑布图像素间差值</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="885"/>
        <source>Anti-Alias Waterfall</source>
        <translation>抗锯齿瀑布图</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="892"/>
        <source>Allow tuning via click and drag (experimental)</source>
        <translation>允许通过点击和拖动进行调谐（实验性功能）。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="899"/>
        <source>Use System Theme</source>
        <translation>使用系统主题</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="906"/>
        <source>Show full screen (F11)</source>
        <translation>全屏模式(F11)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="930"/>
        <source>Frequency Display:</source>
        <translation>频率显示:</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="937"/>
        <source>Units</source>
        <translation>单位</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="945"/>
        <location filename="../settingswidget.ui" line="1039"/>
        <source>None</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="950"/>
        <source>Hz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="955"/>
        <source>kHz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="960"/>
        <source>MHz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="965"/>
        <source>GHz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="973"/>
        <source>Separators:</source>
        <translation>分隔符</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="980"/>
        <source>Decimal</source>
        <translation>十进制</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="990"/>
        <source>Groups</source>
        <translation>组间</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1029"/>
        <source>Underlay Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1036"/>
        <source>No underlay graphics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1049"/>
        <source>Indefinite peak hold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1052"/>
        <source>Peak Hold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1059"/>
        <source>Peak value within the buffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1062"/>
        <source>Peak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1069"/>
        <source>Average value within the buffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1072"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1079"/>
        <source>Underlay Buffer Size:</source>
        <translation>底层缓冲区大小：</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1092"/>
        <source>Size of buffer for spectrum data. Shorter values are more responsive.</source>
        <translation>频谱数据的缓冲区大小。较短的值响应更快。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1111"/>
        <source>Show Bands</source>
        <translation>显示频段</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1151"/>
        <source>Additional Meter Selection:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1164"/>
        <source>Broadcast-style reduction meter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1167"/>
        <source>Reverse Comp Meter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1174"/>
        <source>wfview will automatically calculate command polling. Recommended.</source>
        <translation>wfview将自动计算命令轮询间隔.强烈推荐。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1177"/>
        <source>AutoPolling</source>
        <translation>自动轮询</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1190"/>
        <source>Manual (user-defined) command polling</source>
        <translation>用户自定义命令轮询间隔</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1193"/>
        <source>Manual Polling Interval:</source>
        <translation>设置轮询间隔</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1203"/>
        <source>Sets the polling interval, in ms. Automatic polling is recommended. Serial port and USB port radios should not poll quicker than about 75ms.</source>
        <translation>设置命令轮询间隔，单位ms。强烈建议自动轮询。串口和USB端口的无线电设备轮询间隔不应小于约75ms。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1219"/>
        <source>ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1249"/>
        <source>Color scheme</source>
        <translation>配色方案</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1256"/>
        <source>Preset:</source>
        <translation>预设:</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1278"/>
        <source>Select a color preset here.</source>
        <translation>选择预设方案</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1287"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1292"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1297"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1302"/>
        <source>5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1310"/>
        <source>Revert the selected color preset to the default.</source>
        <translation>将所选的颜色预设恢复为默认值。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1313"/>
        <source>Revert</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1320"/>
        <source>Rename the selected color preset. Max length is 10 characters.</source>
        <translation>重命名所选的颜色预设。最大长度为10个字符。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1323"/>
        <location filename="../settingswidget.cpp" line="2393"/>
        <source>Rename Preset</source>
        <translation>重命名预设方案</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1363"/>
        <source>User-defined Color Editor</source>
        <translation>自定义配色方案编辑器</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1430"/>
        <source>Cluster Spots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1447"/>
        <source>Waterfall Grid</source>
        <translation>瀑布图 网格</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1460"/>
        <location filename="../settingswidget.ui" line="1493"/>
        <location filename="../settingswidget.ui" line="1537"/>
        <location filename="../settingswidget.ui" line="1617"/>
        <location filename="../settingswidget.ui" line="1702"/>
        <location filename="../settingswidget.ui" line="1732"/>
        <location filename="../settingswidget.ui" line="1752"/>
        <location filename="../settingswidget.ui" line="1775"/>
        <location filename="../settingswidget.ui" line="1805"/>
        <location filename="../settingswidget.ui" line="1842"/>
        <location filename="../settingswidget.ui" line="1862"/>
        <location filename="../settingswidget.ui" line="1882"/>
        <location filename="../settingswidget.ui" line="1901"/>
        <location filename="../settingswidget.ui" line="1914"/>
        <location filename="../settingswidget.ui" line="1934"/>
        <location filename="../settingswidget.ui" line="2015"/>
        <location filename="../settingswidget.ui" line="2028"/>
        <location filename="../settingswidget.ui" line="2041"/>
        <location filename="../settingswidget.ui" line="2071"/>
        <location filename="../settingswidget.ui" line="2114"/>
        <location filename="../settingswidget.ui" line="2127"/>
        <location filename="../settingswidget.ui" line="2170"/>
        <location filename="../settingswidget.ui" line="2183"/>
        <location filename="../settingswidget.ui" line="2210"/>
        <location filename="../settingswidget.ui" line="2230"/>
        <location filename="../settingswidget.ui" line="2276"/>
        <location filename="../settingswidget.ui" line="2289"/>
        <location filename="../settingswidget.ui" line="2302"/>
        <source>#AARRGGBB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1500"/>
        <source>Underlay Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1517"/>
        <source>Underlay Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1524"/>
        <source>Underlay Fill Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1564"/>
        <source>Spectrum Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1624"/>
        <source>Waterfall Back</source>
        <translation>瀑布图 背景</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1631"/>
        <source>Underlay Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1638"/>
        <source>Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1655"/>
        <source>Waterfall Axis</source>
        <translation>瀑布图  轴</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1662"/>
        <source>Spectrum Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1689"/>
        <source>Meter High Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1709"/>
        <source>Waterfall Text</source>
        <translation>瀑布图 文字</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1739"/>
        <source>Meter Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1782"/>
        <source>PBT Indicator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1822"/>
        <source>Button On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1829"/>
        <source>Meter Peak Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1849"/>
        <source>Passband</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1869"/>
        <source>Tuning Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1921"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1941"/>
        <source>Spectrum Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1948"/>
        <source>Meter Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="1965"/>
        <source>Plot Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2002"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2058"/>
        <source>Button Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2088"/>
        <source>Underlay Fill Bot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2111"/>
        <source>Color text format is #AARRGGBB, where AA is the &quot;alpha&quot; channel, and value &quot;00&quot; is totally transparent, and &quot;ff&quot; is totally opaque.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2147"/>
        <source>Spectrum Fill Bot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2190"/>
        <source>Meter Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2197"/>
        <source>Spectrum Fill Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2217"/>
        <source>Meter Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2337"/>
        <source>Data Off Modulation Input:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2344"/>
        <source>Modulation Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2347"/>
        <source>Transmit modulation source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2357"/>
        <source>(Data Mod Inputs) DATA1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2364"/>
        <source>Data Modulation Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2367"/>
        <source>Transmit Data-mode modulation input source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2377"/>
        <source>DATA2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2387"/>
        <source>DATA3:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2414"/>
        <source>Press here to set the clock of the radio. The command will be sent to the radio when the seconds go to zero. </source>
        <translation>按此处设置无线电的时钟。当秒数归零时，命令发送给电台。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2417"/>
        <source>Set Clock</source>
        <translation>设置时间</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2424"/>
        <source>Check this box to set the radio&apos;s clock to UTC. Otherwise, the clock will be set to the local timezone of this computer. </source>
        <translation>选中此框以将无线电时钟设置为UTC时间。否则，时钟将设置为此计算机的本地时区。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2427"/>
        <source>Use UTC</source>
        <translation>使用UTC时间</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2434"/>
        <source>Set radio time on connect (takes up to a minute)</source>
        <translation>在连接时设置电台时间（最多需要一分钟）。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2454"/>
        <source>Satellite Ops</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2461"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Click here to adjust the frequency reference on the IC-9700.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;点击此处调整 IC-9700 的频率参考。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2464"/>
        <source>Adjust Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2478"/>
        <source>Manual PTT Toggle</source>
        <translation>手动PTT切换</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2485"/>
        <source>PTT On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2488"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2495"/>
        <source>PTT Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2551"/>
        <source>Enable</source>
        <translation>激活</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2571"/>
        <source>Disable local user controls when in use (restart required)</source>
        <translation>使用时禁用本地用户控制（需要重启）。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2582"/>
        <source>Server Setup</source>
        <translation>服务器设置</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2627"/>
        <location filename="../settingswidget.ui" line="2677"/>
        <location filename="../settingswidget.ui" line="2718"/>
        <source>99999</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2658"/>
        <source>Civ Port</source>
        <translation>Civ 端口</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2680"/>
        <source>50002</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2699"/>
        <source>Audio Port</source>
        <translation>音频端口</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2721"/>
        <source>50003</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2745"/>
        <source>RX Audio Input</source>
        <translation>RX音频输入</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2768"/>
        <source>TX Audio Output</source>
        <translation>TX 音频输出</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2834"/>
        <source>Users</source>
        <translation>用户列表</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2905"/>
        <source>Admin</source>
        <translation>管理员</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2920"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:700;&quot;&gt;Please disconnect from radio to make changes to the server settings&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:700;&quot;&gt;请断开与电台的连接以更改服务器设置。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2965"/>
        <source>Enable RigCtld</source>
        <translation>激活 RigCtld</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="2988"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3028"/>
        <source>Virtual Serial Port</source>
        <translation>虚拟串口</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3047"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this to define a virtual serial port. &lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;On Windows, the virtual serial port can be used to connect to a serial port loopback device, through which other programs can connect to the radio. &lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;On Linux and macOS, the port defined here is a pseudo-terminal device, which may be connected to directly by any program designed for a serial connection. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>使用此项定义一个虚拟串口。

在 Windows 上，虚拟串口可用于连接到串口回环设备，通过该设备，其他程序可以连接到电台。

在 Linux 和 macOS 上，此处定义的端口是一个伪终端设备，任何设计为串行连接的程序都可以直接连接到该端口。</translatorcomment>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使用此项定义一个虚拟串口。&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;在 Windows 上，虚拟串口可用于连接到串口回环设备，通过该设备，其他程序可以连接到电台。&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;在 Linux 和 macOS 上，此处定义的端口是一个伪终端设备，任何设计为串行连接的程序都可以直接连接到该端口。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3050"/>
        <source>Virtual Serial Port Selector</source>
        <translation>选择虚拟串口</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3074"/>
        <source>TCP Server Port</source>
        <translation>TCP 服务端口号</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3094"/>
        <source>Enter port for TCP server, 0 = disabled (restart required if changed)</source>
        <translation>输入TCP服务器的端口，0表示禁用（更改后需要重启）。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3118"/>
        <source>TCI Server Port</source>
        <translatorcomment>TCI是什么？</translatorcomment>
        <translation>TCI 服务端口号</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3135"/>
        <source>Enter port for TCI server 0 = disabled (restart required if changed)</source>
        <translation>输入TCI服务器的端口，0表示禁用（更改后需要重启）。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3159"/>
        <source>Waterfall Format</source>
        <translation>瀑布图 格式</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3167"/>
        <source>Default</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3172"/>
        <source>Single (network)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3177"/>
        <source>Multi (serial)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3185"/>
        <source>Only change this if you are absolutely sure you need it (connecting to N1MM+ or similar)</source>
        <translation>仅在您完全确定需要时更改此项（例如连接到 N1MM+ 或类似软件）。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3209"/>
        <source>Enable USB Controllers</source>
        <translation>激活USB控制器</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3219"/>
        <source>Setup USB Controller</source>
        <translation>设置USB控制器</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3239"/>
        <source>Reset Buttons</source>
        <translation>重置按钮</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3246"/>
        <source>Only reset buttons/commands if you have issues. </source>
        <translation>仅在遇到问题时重置按钮/命令。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3290"/>
        <source>This page contains configuration for DX Cluster, either UDP style broadcast (from N1MM+/DXlog) or TCP connection to your favourite cluster</source>
        <translation>此页面包含 DX 集群的配置，支持 UDP 广播方式（来自 N1MM+/DXlog）或 TCP 连接到您喜爱的集群。</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3299"/>
        <source>TCP Cluster Connection</source>
        <translation>TCP 集群连接</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3310"/>
        <source>Server Name</source>
        <translation>服务器名</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3335"/>
        <source>Add/Update</source>
        <translation>添加/更新</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3348"/>
        <source>Del</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3357"/>
        <source>Server Port</source>
        <translation>服务器端口号</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3364"/>
        <location filename="../settingswidget.ui" line="3466"/>
        <source>00000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3395"/>
        <source>Spot Timeout (minutes)</source>
        <translation>信号超时时间(分钟)</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3402"/>
        <source>0000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3414"/>
        <source>Connect</source>
        <translation>联接</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3421"/>
        <source>Disconnect</source>
        <translation>断开</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3448"/>
        <source>UDP Broadcast Connection</source>
        <translation>UDP 广播 连接</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3459"/>
        <source>UDP Port</source>
        <translation>UDP 端口号</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3490"/>
        <source>Show Skimmer Spots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3521"/>
        <source>This page contains experimental features. Use at your own risk.</source>
        <translation>此页面包含实验性功能，请自行承担风险使用</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3532"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This button runs debug functions, and is provided as a convenience for programmers. The functions executed are under:&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; color:#ffff55;&quot;&gt;void&lt;/span&gt;&lt;span style=&quot; color:#55ff55;&quot;&gt;wfmain&lt;/span&gt;&lt;span style=&quot; color:#aaaaaa;&quot;&gt;::&lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;on_debugBtn_clicked&lt;/span&gt;&lt;span style=&quot; color:#aaaaaa;&quot;&gt;()&lt;/span&gt;&lt;/p&gt;&lt;p&gt;in wfmain.cpp.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;此按钮运行调试功能，作为程序员的便捷工具。执行的功能位于：&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; color:#ffff55;&quot;&gt;void&lt;/span&gt;&lt;span style=&quot; color:#55ff55;&quot;&gt;wfmain&lt;/span&gt;&lt;span style=&quot; color:#aaaaaa;&quot;&gt;::&lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;on_debugBtn_clicked&lt;/span&gt;&lt;span style=&quot; color:#aaaaaa;&quot;&gt;()&lt;/span&gt;&lt;/p&gt;&lt;p&gt;in wfmain.cpp.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3535"/>
        <source>Debug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3538"/>
        <source>Ctrl+Alt+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3609"/>
        <source>Save Settings</source>
        <translation>保存配置</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3616"/>
        <source>Revert to Default</source>
        <translation>恢复到默认值</translation>
    </message>
    <message>
        <location filename="../settingswidget.ui" line="3636"/>
        <source>Connect to Radio</source>
        <translation>连接电台</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="46"/>
        <source>Radio Access</source>
        <translation>连接电台</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="47"/>
        <source>User Interface</source>
        <translation>用户界面</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="48"/>
        <source>Radio Settings</source>
        <translation>电台设置</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="49"/>
        <source>Radio Server</source>
        <translation>远程设置</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="50"/>
        <source>External Control</source>
        <translation>额外控制项</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="51"/>
        <source>DX Cluster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="52"/>
        <source>Experimental</source>
        <translation>实验性特性</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1054"/>
        <source>
Server audio output device does not exist, please check.
Transmit audio will NOT work until this is corrected
**** please disable the server if not required ****</source>
        <translation>
服务器音频输出设备不存在，请检查。
音频传输在此问题解决之前将无法工作。
*** 如果不需要，请禁用服务器。****</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1065"/>
        <source>
Server audio input device does not exist, please check.
Receive audio will NOT work until this is corrected
**** please disable the server if not required ****</source>
        <translation>
服务器音频输入设备不存在，请检查。
音频输入在此问题解决之前将无法工作。
*** 如果不需要，请禁用服务器。****</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1353"/>
        <source>Admin User</source>
        <translation>管理员用户</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1353"/>
        <source>Normal User</source>
        <translation>普通用户</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1353"/>
        <source>Normal with no TX</source>
        <comment>Monitor only</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1361"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1623"/>
        <source>Manual port assignment</source>
        <translation>手动设置端口</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1624"/>
        <source>Enter serial port assignment:</source>
        <translation>输入串口分配：</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="1626"/>
        <source>/dev/device</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="2229"/>
        <source>Specify Opacity</source>
        <translation>指定透明度</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="2230"/>
        <source>You specified an opacity value of 0. 
Do you want to change it? (0=transparent, 255=opaque)</source>
        <translation>您指定了透明度值为 0。
您想更改它吗？（0=透明，255=不透明）</translation>
    </message>
    <message>
        <location filename="../settingswidget.cpp" line="2394"/>
        <source>Preset Name (10 characters max):</source>
        <translation>预设名称（最多 10 个字符）：</translation>
    </message>
</context>
<context>
    <name>wfmain</name>
    <message>
        <location filename="../wfmain.ui" line="17"/>
        <source>wfmain</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="197"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Turns the radio on&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>打开电源</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="200"/>
        <source>Power On</source>
        <translation>电台开机</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="210"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Turns the radio off&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>关闭电源</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="213"/>
        <source>Power Off</source>
        <translation>电台关机</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="247"/>
        <source>Tuning Dial</source>
        <translation>调谐旋钮</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="272"/>
        <source>Tuning Step Selection possibly. Or not...</source>
        <translation>可能是调谐步进选择。或者不是...</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="275"/>
        <source>Tuning Step Selection</source>
        <translation>调谐步进选择</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="285"/>
        <source>Frequency Lock</source>
        <translation>锁定频率</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="288"/>
        <source>F Lock</source>
        <translation>频率锁</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="303"/>
        <source>R I T Dial</source>
        <translation>接收增量调谐旋钮</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="331"/>
        <source>R I T Enable</source>
        <translation>激活RIT</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="334"/>
        <source>RIT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="400"/>
        <source>RX RF Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="403"/>
        <source>RF Gain</source>
        <translation>调整射频增益</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="406"/>
        <source>Receiver RF Gain</source>
        <translation>接收器射频增益</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="431"/>
        <source>RF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="463"/>
        <source>RX AF Gain</source>
        <translation>调整音量输出增益</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="466"/>
        <source>AF Gain</source>
        <translatorcomment>调整音量输出增益</translatorcomment>
        <translation>调整音量输出增益</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="469"/>
        <source>Receive Audio Level. Sets rig volume on USB rigs, and sets PC volume on LAN rigs.</source>
        <translation>接收音频级别。设置 USB 无线电设备的设备音量，以及 LAN 无线电设备的电脑音量。</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="488"/>
        <source>AF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="520"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Squelch&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>静噪</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="523"/>
        <source>Squelch</source>
        <translation>静噪</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="526"/>
        <source>Squelch control. Top is fully-muted, bottom is wide open.</source>
        <translation>静噪控制。顶部为完全静音，底部为完全开放。</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="545"/>
        <source>SQ</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="577"/>
        <source>Mic Gain</source>
        <translation>麦克风 增益</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="580"/>
        <source>Transmit Audio Gain</source>
        <translation>发射音频增益</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="583"/>
        <source>Sets the gain for the transmit audio source, for example mic gain or accessory port gain</source>
        <translation>设置发射音频的增益，例如麦克风增益或accessory por增益。</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="602"/>
        <source>Mic</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="634"/>
        <location filename="../wfmain.ui" line="637"/>
        <source>Transmit Power</source>
        <translation>发射功率</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="640"/>
        <source>Transmit power level</source>
        <translation>发射功率级别</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="659"/>
        <source>TX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="688"/>
        <source>Set the rado monitor level</source>
        <translation>设置电台监听级别</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="710"/>
        <source>&lt;a href=&apos;#&apos;&gt;Mon&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="756"/>
        <source>Noise Blanker</source>
        <translation>调整噪声剪切器</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="759"/>
        <source>NB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="766"/>
        <source>Noise Reduction</source>
        <translation>调整降噪电平</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="769"/>
        <source>NR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="776"/>
        <source>IP+ Funcion</source>
        <translation>IP+ 功能</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="779"/>
        <source>IP+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="793"/>
        <source>Digi-Sel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="796"/>
        <source>DS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="803"/>
        <source>Compressor</source>
        <translation>语音压缩</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="806"/>
        <source>CMP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="813"/>
        <source>Vox Enable</source>
        <translation>发信机声控</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="816"/>
        <source>VOX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="847"/>
        <source>Transmit and Receive button</source>
        <translation>发射按钮</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="850"/>
        <source>Transmit</source>
        <translation>发射</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="857"/>
        <source>Enable the Automatic Antenna Tuner</source>
        <translation>启用自动天调(ATU)</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="860"/>
        <location filename="../wfmain.ui" line="866"/>
        <source>Enable ATU</source>
        <translation>天调</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="863"/>
        <source>Enable or disable the automatic antenna tuner</source>
        <translation>启用或禁用自动天调(ATU)</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="876"/>
        <source>Start the automatic antenna tuner cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="879"/>
        <source>Tune</source>
        <translation>调谐</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="889"/>
        <source>Press to bring up the CW Sender</source>
        <translation>打开CW操作面板</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="892"/>
        <source>CW</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="902"/>
        <source>Show the repeater tone and offset window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="905"/>
        <source>Rpt/Split</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="915"/>
        <source>Memories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="941"/>
        <source>Scope Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="972"/>
        <source>Dual Watch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="997"/>
        <source>Dual Scope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1016"/>
        <source>Main/Sub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1029"/>
        <source>Split</source>
        <translation>异频</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1042"/>
        <source>Main&lt;&gt;Sub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1052"/>
        <source>Main=Sub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1066"/>
        <source>Preamp/Att</source>
        <translation>P.AMP/ATT</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1089"/>
        <source>Preamp:</source>
        <translation>P.AMP</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1096"/>
        <source>Preamp selector</source>
        <translation>前置放大器选择</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1116"/>
        <source>Attenuator:</source>
        <translation>ATT</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1123"/>
        <source>Attenuator selector</source>
        <translation>衰减器选择</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1138"/>
        <source>Antenna</source>
        <translation>天线</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1159"/>
        <source>Antenna port selector</source>
        <translation>选择天线端口</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1172"/>
        <source>RX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1229"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1239"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1249"/>
        <source>Save Settings</source>
        <translation>保存设置</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1259"/>
        <source>Radio Status</source>
        <translation>电台状态</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1269"/>
        <source>Log</source>
        <translation>系统日志</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1279"/>
        <source>Bands</source>
        <translation>频段选择</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1289"/>
        <source>Frequency</source>
        <translation>频率输入</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1299"/>
        <source>Rig Creator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1322"/>
        <source>Connect to Radio</source>
        <translation>连接电台</translation>
    </message>
    <message>
        <location filename="../wfmain.ui" line="1350"/>
        <source> Exit Program</source>
        <translation>退出应用</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="2819"/>
        <source>wfview</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="2820"/>
        <source>Are you sure you wish to reset the USB controllers?</source>
        <translation>您确定要重置USB控制器吗？</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4106"/>
        <location filename="../wfmain.cpp" line="4637"/>
        <location filename="../wfmain.cpp" line="5147"/>
        <source>Don&apos;t ask me again</source>
        <translation>不需要再问我</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4107"/>
        <source>Don&apos;t ask me to confirm exit again</source>
        <translation>退出前不要再让我确认。</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4109"/>
        <source>Are you sure you wish to exit?
</source>
        <translation>您确定要退出吗？</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4200"/>
        <source>Revert settings</source>
        <translation>恢复设置</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4201"/>
        <source>Are you sure you wish to reset all wfview settings?
If so, wfview will exit and you will need to start the program again.</source>
        <translation>您确定要重置所有 wfview 设置吗？
如果是，wfview 将退出，您需要重新启动程序。</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4639"/>
        <source>Power</source>
        <translation>电源</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="4640"/>
        <source>Power down the radio?
</source>
        <translation>电台要关机吗？</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="5148"/>
        <source>Don&apos;t ask me to confirm memories again</source>
        <translation>不要再要求我确认记忆设置。</translation>
    </message>
    <message>
        <location filename="../wfmain.cpp" line="5150"/>
        <source>Memories are considered an experimental feature,
Please make sure you have a full backup of your radio before making changes.
Are you sure you want to continue?
</source>
        <translation>存储功能被视为实验性功能，
在进行更改之前，请确保已对电台设备进行完整备份。
您确定要继续吗？</translation>
    </message>
</context>
</TS>
